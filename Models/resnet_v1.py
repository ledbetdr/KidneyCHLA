"""
Kidney CNN architecture: Imitating Microsoft's Residual Network w/ Facebook's
modifications.
"""
from keras.models import Sequential, Graph
from keras.layers.core import Flatten, Dense, Dropout, Activation, Merge
from keras.layers.normalization import BatchNormalization
from keras.layers.convolutional import Convolution2D, MaxPooling2D, AveragePooling2D, ZeroPadding2D
from keras.layers.advanced_activations import LeakyReLU
from keras.regularizers import l2, activity_l2


def conv(model, input_name, name, n_filters, filter_size, border_mode='valid',
         activation='linear', conv_kwargs=None):
    """Convolution with BatchNormalization and flexible activation. Same
    padding is done instead of using keras' implementation of
    border_mode='same' because zero_padding with border_mode='valid' was
    experimentally found to be faster, and more memory efficient.

    Parameters
    -------
    model: Kears.layers.containers.Graph()
        keras graph container for constructing models. Uses `.add_node` attr.

    input_name: str
        Name of layer within `model` to be fed as input to layer

    name: str
        Name of current layer to specify within `model`

    n_filters: int
        Number of filters

    filter_size: (Ky, Kx)
        Size of kernels

    border_mode: str, tuple of padding (Py, Px)
        For str, `border_mode` must be either `same` or `valid`. If `same`,
        will zero_pad the input to produce the same output feature maps. If
        `valid` (default), will apply default valid convolutions. If tuple,
        will apply padding of (Py, Px) to the input.

    activation: str
        Activation to apply after BatchNormalization. Activation must be
        one of ['softmax', 'softplus', 'relu', 'tanh', 'sigmoid',
        'hard_sigmoid', 'linear', 'leakyrelu']

    conv_kwargs: dict
        Additional keyword arguments to pass to kera's Convolution2D.
        For example, for stride=2, use conv_kwargs = {'subsample': (2, 2)}

    Returns
    -------
    model, last_input
        model: keras Graph model with added layer
        last_input: (str) Name of last layer added to model
    """
    if conv_kwargs is None:
        conv_kwargs = {}

    # padding
    # Kera's default `border_mode=same` is slower and requires more memory
    # than a simple valid convolution with input padding
    if border_mode == 'valid':
        pass
    else:
        if border_mode == 'same':
            if conv_kwargs.get('subsample', (1, 1)) != (1, 1):
                raise RuntimeError("For padding=same, must have subsample=(1, 1)")

            p = ((filter_size[0] - 1) / 2, (filter_size[1] - 1) / 2)
        else:
            p = border_mode

        model.add_node(ZeroPadding2D(padding=p), input=input_name, name=(name + '_zp'))
        input_name = name + '_zp'

    # convolution layer
    model.add_node(Convolution2D(n_filters, filter_size[0], filter_size[1], **conv_kwargs), input=input_name, name=(name + '_conv'))
    last_input = name + '_conv'

    # BatchNormalization
    model.add_node(BatchNormalization(), input=last_input, name=(name + 'bn'))
    last_input = name + 'bn'

    # activation
    if activation == 'linear':
        pass
    elif activation == 'leakyrelu':
        model.add_node(LeakyReLU(alpha=0.3), input=last_input, name=(name + 'lrelu'))
        last_input = name + 'lrelu'
    else:
        model.add_node(Activation(activation), input=last_input, name=(name + '%s' % activation))
        last_input = name + '%s' % activation

    return model, last_input


def res_bottleneck(model, input_name, name, input_n_filters,
                   downsample_last=False, do_init_1x1conv=False,
                   w_reg=l2(0.0001)):
    """
    Microsoft Residual Bottlneck architecture.

    Parameters
    -------
    model: kears.layers.containers.Graph()
        keras graph container for constructing models. Uses `.add_node` attr.

    input_name: str
        Name of layer within `model` to be fed as input to layer

    name: str
        Name of current layer to specify within `model`

    input_n_filters: int
        Number of filters from last bottleneck layer or input conv layer.
        Bottleneck decreases filters by input_n_filters / 2 when decreasing
        computation and expands filters by input_n_filters * 2 when increasing
        parameters.

    downsample_last: bool, defualt=False
        Apply spatial downsampling by 2x (using conv stride 2) to the last
        convolution and input of the bottleneck architecture.

    do_init_1x1conv: bool, default=False
        If no downsampling, but want to increase/decrease # filters, use this.

    w_reg: keras.regularizers.WeightRegularizer

    Returns
    -------
    model, last_input
        model: keras Graph model with added layer
        last_input: (str) Name of last layer added to model
    """
    if downsample_last:
        conv_kwargs = {'subsample': (2, 2), 'W_regularizer': w_reg}

        out_filters = 2 * input_n_filters
        # 1x1 filters, 2 stride convolutions on input to downsample & expand filters
        model, last_input = conv(model, input_name, name + '_branch1', out_filters, (1, 1), conv_kwargs=conv_kwargs)
        new_input_name = last_input
    else:
        conv_kwargs = {'subsample': (1, 1), 'W_regularizer': w_reg}
        out_filters = input_n_filters
        if do_init_1x1conv:
            # 1x1, 1 stride convolution on input to expand filters
            model, last_input = conv(model, input_name, name + '_branch1', out_filters, (1, 1), conv_kwargs=conv_kwargs)
            new_input_name = last_input
        else:
            new_input_name = input_name

    conv_kwargs2 = {'W_regularizer': w_reg}

    # compute residual branch -- (2) 3x3 same convs w/ bottleneck architecture
    model, last_input = conv(model, last_input, name + '_branch2a', out_filters / 4, (1, 1), activation='relu', conv_kwargs=conv_kwargs)

    model, last_input = conv(model, last_input, name + '_branch2b', out_filters / 4, (3, 3), border_mode='same', activation='relu', conv_kwargs=conv_kwargs2)

    model, last_input = conv(model, last_input, name + '_branch2c', out_filters, (1, 1), conv_kwargs=conv_kwargs2)

    # residual -- sum input layer and output of bottleneck and apply relu
    model.add_node(Activation('relu'), inputs=[last_input, new_input_name], name=name, merge_mode='sum')
    last_input = name

    return model, last_input, out_filters

def n_res_bottlenecks(model, input_name, name, n, input_n_filters,
                      downsample_last=True):
    """
    Applies `n` residual bottlenecks layers

    Parameters
    -------
    model: kears.layers.containers.Graph()
        keras graph container for constructing models. Uses `.add_node` attr.

    input_name: str
        Name of layer within `model` to be fed as input to layer

    name: str
        Name of current layer to specify within `model`

    n: int
        Number of residual bottlenecks to apply

    input_n_filters: int
        Number of filters from last bottleneck layer or input conv layer.
        Bottleneck decreases filters by input_n_filters / 2 when decreasing
        computation and expands filters by input_n_filters * 2 when increasing
        parameters.

    downsample_last: bool, default=True
        Whether to downsample the last bottleneck conv or not. If True,
        this will increase number of filter size by 2.

    Returns
    -------
    model, last_input
        model: keras Graph model with added layer
        last_input: (str) Name of last layer added to model
    """

def generate_model(data_shape,
                   n_features,                      # number of features to concat to dense layer 1
                   n_outputs,                       # number of outputs to regress
                   NUM_HIDDEN_UNITS    = 256,       # number of dense layer nodes in the final layers
                   LAYER_1_FILTERS     = 64,        # Number of filters in conv1 block
                   WREGULARIZER        = 'default', # Weight regularizer for conv/dense
                   BREGULARIZER        = None,      # Bias regularizer for conv
                   AREGULARIZER        = None,      # Bias regularizer for dense
                   weights_path        = None,      # load previously learned weights
                   ):

    if WREGULARIZER == 'default':
        WREGULARIZER = l2(0.0004)
    else:
        WREGULARIZER = None

    # prepare model
    model = Graph()

    # add inputs
    model.add_input(name='data', input_shape=data_shape)
    model.add_input(name='features', input_shape=(n_features,))

    # initial convolution
    model, last_input = conv(model, 'data', 'init_conv', LAYER_1_FILTERS, (7, 7), border_mode=(3, 3), activation='relu', conv_kwargs={'subsample': (2, 2)})

    # res layers



    if weights_path is not None:
        model.load_weights(weights_path)

    return model

if __name__ == '__main__':
    from keras.utils import layer_utils, visualize_util

    # generate modelx`
    model = generate_model((3, 275, 275), 1, 1)

    # print model summary
    layer_utils.model_summary(model)

    # print model layout
    visualize_util.plot(model, to_file='%s.png' % __file__)
