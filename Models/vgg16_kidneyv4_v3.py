"""
Kidney CNN architecture: Adapted from VGG16
"""
from keras.models import Sequential, Graph
from keras.layers.core import Flatten, Dense, Dropout
from keras.layers.convolutional import Convolution2D, MaxPooling2D, ZeroPadding2D
from keras.layers.advanced_activations import LeakyReLU
from keras.regularizers import l2, activity_l2


def build_conv_group(model, input_name, base_name, n_filters, filter_size, pool_size, conv_kwargs=None):
    if conv_kwargs is None:
        conv_kwargs = {}

    def get_name(n):
        return '%s_%i' % (base_name, n)
    n = 0
    model.add_node(ZeroPadding2D(padding=(1, 1)), name=get_name(n), input=input_name)
    n += 1
    model.add_node(Convolution2D(n_filters, filter_size, filter_size, **conv_kwargs), name=get_name(n), input=get_name(n-1))
    n += 1
    model.add_node(LeakyReLU(alpha=0.3), name=get_name(n), input=get_name(n-1))
    n += 1
    model.add_node(ZeroPadding2D(padding=(1, 1)), name=get_name(n), input=get_name(n-1))
    n += 1
    model.add_node(Convolution2D(n_filters, filter_size, filter_size, **conv_kwargs), name=get_name(n), input=get_name(n-1))
    n += 1
    model.add_node(LeakyReLU(alpha=0.3), name=get_name(n), input=get_name(n-1))
    n += 1
    model.add_node(MaxPooling2D(pool_size=(pool_size, pool_size), strides=(2, 2)), name=get_name(n), input=get_name(n-1))
    return get_name(n)


def generate_model(data_shape,
                   n_features,                          # number of features to concat to dense layer 1
                   n_outputs,                           # number of outputs to regress
                   NUM_HIDDEN_UNITS    = 384,           # number of dense layer nodes in the final layers
                   X_KERNEL_SIZE       = 3,             # X-size of conv. filters
                   Y_KERNEL_SIZE       = 3,             # Y-size of conv. filters
                   X_POOL_SIZE         = 2,             # X-size of pool
                   Y_POOL_SIZE         = 2,             # Y-size of pool
                   X_STRIDE            = 2,             # X-stride of pool
                   Y_STRIDE            = 2,             # Y-stride of pool
                   LAYER_1_FILTERS     = 64,            # Number of filters in conv1 block
                   LAYER_2_FILTERS     = 128,           # Number of filters in conv2 block
                   LAYER_3_FILTERS     = 256,           # Number of filters in conv3 block
                   LAYER_4_FILTERS     = 256,           # Number of filters in conv4 block
                   BORDER_MODE         = 'valid',       # Conv. border
                   WREGULARIZER        = 'default',     # Weight regularizer for conv/dense
                   BREGULARIZER        = None,          # Bias regularizer for conv
                   AREGULARIZER        = None,          # Bias regularizer for dense
                   weights_path        = None,          # load previously learned weights
                   ):

    if WREGULARIZER == 'default':
        WREGULARIZER = l2(0.0001)
    else:
        WREGULARIZER = None

    # common keyword args for conv & dense layers
    conv_kwargs = {'border_mode': BORDER_MODE, 'init': 'glorot_uniform',
                   'W_regularizer': WREGULARIZER, 'b_regularizer': BREGULARIZER}
    dense_kwargs = {'W_regularizer': WREGULARIZER, 'activity_regularizer': AREGULARIZER}

    # prepare model
    model = Graph()

    # add inputs
    model.add_input(name='data', input_shape=data_shape)
    model.add_input(name='features', input_shape=(n_features,))

    cv1 = build_conv_group(model, 'data', 'conv1', LAYER_1_FILTERS, Y_KERNEL_SIZE, Y_POOL_SIZE, conv_kwargs=conv_kwargs)
    cv2 = build_conv_group(model, cv1, 'conv2', LAYER_2_FILTERS, Y_KERNEL_SIZE, Y_POOL_SIZE, conv_kwargs=conv_kwargs)
    cv3 = build_conv_group(model, cv2, 'conv3', LAYER_3_FILTERS, Y_KERNEL_SIZE, Y_POOL_SIZE, conv_kwargs=conv_kwargs)
    cv4 = build_conv_group(model, cv3, 'conv4', LAYER_4_FILTERS, Y_KERNEL_SIZE, Y_POOL_SIZE, conv_kwargs=conv_kwargs)

    # dense layers
    model.add_node(Flatten(), name='flatten', input=cv4)
    model.add_node(Dense(NUM_HIDDEN_UNITS, activation='relu', **dense_kwargs), name='dense1', inputs=['flatten', 'features'], merge_mode='concat')
    model.add_node(Dropout(0.25), name='dense1_do', input='dense1')
    model.add_node(Dense(NUM_HIDDEN_UNITS, activation='relu', **dense_kwargs), name='dense2', input='dense1_do')
    model.add_node(Dropout(0.35), name='dense2_do', input='dense2')
    model.add_node(Dense(n_outputs, **dense_kwargs), name='dense3', input='dense2_do')
    model.add_output(name='output', input='dense3')

    if weights_path is not None:
        model.load_weights(weights_path)

    return model

if __name__ == '__main__':
    from keras.utils import layer_utils, visualize_util

    # generate model
    model = generate_model((3, 275, 275), 1, 1)

    # print model summary
    layer_utils.model_summary(model)

    # print model layout
    visualize_util.plot(model, to_file='%s.png' % __file__)
