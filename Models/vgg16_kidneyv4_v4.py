"""
Kidney CNN architecture v1: Adapted from VGG16

v4_v4: Added batch normalization
"""
from keras.models import Sequential, Graph
from keras.layers.core import Flatten, Dense, Dropout
from keras.layers.normalization import BatchNormalization
from keras.layers.convolutional import Convolution2D, MaxPooling2D, AveragePooling2D, ZeroPadding2D
from keras.layers.advanced_activations import LeakyReLU
from keras.regularizers import l2, activity_l2


def get_name(base_name, n):
    return '%s_%i' % (base_name, n)


def build_conv(model, input_name, base_name, n_filters, filter_size, conv_kwargs=None):
    if conv_kwargs is None:
        conv_kwargs = {}

    n = 0
    model.add_node(ZeroPadding2D(padding=(1, 1)), name=get_name(base_name, n), input=input_name)
    n += 1
    model.add_node(Convolution2D(n_filters, filter_size, filter_size, **conv_kwargs), name=get_name(base_name, n), input=get_name(base_name, n-1))
    n += 1
    model.add_node(BatchNormalization(), name=get_name(base_name, n), input=get_name(base_name, n-1))
    n += 1
    model.add_node(LeakyReLU(alpha=0.3), name=get_name(base_name, n), input=get_name(base_name, n-1))

    return get_name(base_name, n)


def build_conv_group_no_mp(model, input_name, base_name, n_filters, filter_size, conv_kwargs=None):
    n = 0
    cv = build_conv(model, input_name, get_name(base_name, n), n_filters, filter_size, conv_kwargs=conv_kwargs)
    n += 1
    cv = build_conv(model, cv, get_name(base_name, n), n_filters, filter_size, conv_kwargs=conv_kwargs)
    n += 1
    cv = build_conv(model, cv, get_name(base_name, n), n_filters, filter_size, conv_kwargs=conv_kwargs)
    n += 1
    cv = build_conv(model, cv, get_name(base_name, n), n_filters, filter_size, conv_kwargs=conv_kwargs)
    return cv


def build_conv_group(model, input_name, base_name, n_filters, filter_size, pool_size, conv_kwargs=None):
    cv = build_conv_group_no_mp(model, input_name, base_name, n_filters, filter_size)
    model.add_node(MaxPooling2D(pool_size=(pool_size, pool_size), strides=(2, 2)), name=base_name + '_mp', input=cv)
    return base_name + '_mp'


def generate_model(data_shape,
                   n_features,                      # number of features to concat to dense layer 1
                   n_outputs,                       # number of outputs to regress
                   NUM_HIDDEN_UNITS    = 256,       # number of dense layer nodes in the final layers
                   X_KERNEL_SIZE       = 3,         # X-size of conv. filters
                   Y_KERNEL_SIZE       = 3,         # Y-size of conv. filters
                   X_POOL_SIZE         = 3,         # X-size of pool
                   Y_POOL_SIZE         = 3,         # Y-size of pool
                   LAYER_1_FILTERS     = 64,        # Number of filters in conv1 block
                   LAYER_2_FILTERS     = 128,       # Number of filters in conv2 block
                   LAYER_3_FILTERS     = 256,       # Number of filters in conv3 block
                   LAYER_4_FILTERS     = 512,       # Number of filters in conv4 block
                   BORDER_MODE         = 'valid',   # Conv. border
                   WREGULARIZER        = 'default', # Weight regularizer for conv/dense
                   BREGULARIZER        = None,      # Bias regularizer for conv
                   AREGULARIZER        = None,      # Bias regularizer for dense
                   weights_path        = None,      # load previously learned weights
                   ):

    if WREGULARIZER == 'default':
        WREGULARIZER = l2(0.0001)
    else:
        WREGULARIZER = None

    # common keyword args for conv & dense layers
    conv_kwargs = {'border_mode': BORDER_MODE, 'init': 'glorot_uniform',
                   'W_regularizer': WREGULARIZER, 'b_regularizer': BREGULARIZER}

    dense_kwargs = {'W_regularizer': WREGULARIZER, 'activity_regularizer': AREGULARIZER}

    # prepare model
    model = Graph()

    # add inputs
    model.add_input(name='data', input_shape=data_shape)
    model.add_input(name='features', input_shape=(n_features,))

    # initial convolution
    model.add_node(ZeroPadding2D(padding=(1, 1)), name='conv_input_zp', input='data')
    model.add_node(Convolution2D(32, 3, 3, subsample=(1, 1), **conv_kwargs), name='conv_input', input='conv_input_zp')
    model.add_node(BatchNormalization(), name='conv_input_bn', input='conv_input')
    model.add_node(LeakyReLU(alpha=.3), name='conv_input_relu', input='conv_input_bn')
    model.add_node(MaxPooling2D(pool_size=(Y_POOL_SIZE, X_POOL_SIZE), strides=(2, 2)), name='conv_input_mp', input='conv_input_relu')

    cv1 = build_conv_group(model, 'conv_input_mp', 'conv1', LAYER_1_FILTERS, Y_KERNEL_SIZE, Y_POOL_SIZE, conv_kwargs=conv_kwargs)
    cv2 = build_conv_group(model, cv1, 'conv2', LAYER_2_FILTERS, Y_KERNEL_SIZE, Y_POOL_SIZE, conv_kwargs=conv_kwargs)
    cv3 = build_conv_group(model, cv2, 'conv3', LAYER_3_FILTERS, Y_KERNEL_SIZE, Y_POOL_SIZE, conv_kwargs=conv_kwargs)
    cv4 = build_conv_group_no_mp(model, cv3, 'conv4', LAYER_4_FILTERS, Y_KERNEL_SIZE, conv_kwargs=conv_kwargs)

    # average pool after convolutions -- hard coded for inputs of 275x275
    model.add_node(AveragePooling2D(pool_size=(16, 16)), name='avg_pool', input=cv4)

    # dense layers
    model.add_node(Flatten(), name='flatten', input='avg_pool')
    model.add_node(Dropout(0.25), name='dense1_do', input='flatten')
    model.add_node(Dense(NUM_HIDDEN_UNITS, activation='relu', **dense_kwargs), name='dense2', inputs=['dense1_do', 'features'])
    model.add_node(Dense(n_outputs, **dense_kwargs), name='dense3', input='dense2')
    model.add_output(name='output', input='dense3')

    if weights_path is not None:
        model.load_weights(weights_path)

    return model

if __name__ == '__main__':
    from keras.utils import layer_utils, visualize_util

    # generate modelx`
    model = generate_model((3, 275, 275), 1, 1)

    # print model summary
    layer_utils.model_summary(model)

    # print model layout
    visualize_util.plot(model, to_file='%s.png' % __file__)
