# KidneyCHLA - Kidney Function Prediction with Children's Hospital Los Angeles
KidneyCHLA is a project with the Children’s Hospital Los Angeles (CHLA) whose goal is to predict future kidney function in patients with chronic kidney disease.

### **Overview**

1. [Introduction](#introduction)

2. [Data](#data)

3. [Truth](#truth)

4. [Pre-processing and Data Augmentation](#pre-processing-and-data-augmentation)

5. [Network Architecture](#network-architecture)

6. [Training](#training)

7. [Results](#results)

### **Introduction**

The goal of this project is to predict future kidney function in patients with chronic kidney disease using high-resolution digital pathology scans of their kidney biopsies. Kidney biopsies were taken from participants of the [Neptune study](http://www.ncbi.nlm.nih.gov/pubmed/23325076), a longitudinal collaborative consortium whose goal is to set up infrastructure for observing predictors for idiopathic kidney disease (umbrella of kidney diseases whose origin is unknown). The knowledge of future kidney function is desirable as it can identify high-risk patients and influence treatment decisions, reducing the likelihood of irreversible kidney decline. 

#### *Background*

The measure of kidney function is estimated by how much blood passes through the [glumerli](https://en.wikipedia.org/wiki/Glomerulus_(kidney)) (see figure below) each minute, also known as the Glomerular Filtration Rate (GFR). The glomeruli serve as tiny filters that distills waste products from the blood. In practice, however, exact GFR is difficult to measure and thus an approximation to the GFR is obtained (estimated Glomerular Filtration Rate) using creatinine measurements. Consequently, eGFR is commonly used as the indicator of kidney function and health. Our goal is then to develop/train an algorithm to predict future eGFR.


!["Juxtaglomerular Apparatus and Glomerulus" by OpenStax College - Anatomy & Physiology, Connexions Web site. http://cnx.org/content/col11496/1.6/, Jun 19, 2013.. Licensed under CC BY 3.0 via Commons](images/normal_glomerulus.png)

#### *Motivation*

Previous works by Dr. Lemley [[1](http://ndt.oxfordjournals.org/content/23/1/213.full.pdf), [2](Morphometry Predicts Early GFR Change in Idiopathic Nephrotic Syndrome: TO-BE-PUBLISHED)] have correlated glomeruli morphometry to the change in eGFR by hand-measuring properties such as the fractional interstitial area and average glomerular tuft volume. Moreover, there are also obvious visual differences in glomeruli between patients with healthy and declining kidney function, further indicating the presence of an interaction between kidney function and glomeruli shape. In order to automate the extraction of visual information contained within the digital pathology, a Convolutional Neural Network was implemented to exploit the correlations between the morphometry of cells present in kidney biopsies and future kidney function.

### **Data**

This project utilized a subset of the [NEPTUNE dataset](http://www.ncbi.nlm.nih.gov/pubmed/23325076). There were over 80 patients available at various stages of longitudinal study. Initial biopsies were captured through a variety of staining techniques during their first visit (and thus slides of kidney cells came in variety of colors). Follow up visits collected eGFR measurements at 4 month intervals for 5 years, which are used as labels for supervised training. A kidney slide’s resolution is on the orders of 150000x50000x3 pixels with each pixel measuring 20 microns. An example of a patient’s slide is shown below (not the actual data). This raw form of the data is [pre-processed](#pre-processing-and-data-augmentation) before training the network.


![http://www.dartmouth.edu/~anatomy/Histo/lab_5/renal/DMS152/15.gif](images/example_slide_w_zoom.png)

### **Truth**

Estimated Glomerular Filtration Rates were measured beginning at 4 month and increasing to 6 month intervals. The truth provided to the network was a single eGFR measurement at some time interval (e.g. at 12 month). For the initial prototype, a single time interval was selected for simplicity (12 months). Future work will incorporate a vectorized regression target to enable a broader range of clinically significant eGFR predictions (e.g. [4, 8, 12, 18, 24, 30, 36 month]).

Code Blurb: The truth was scraped from notes from Dr. Lemley’s study and aggregated into a csv using [preprocessTruth.py](Code/preprocessTruth.py)

### **Pre-processing and Data Augmentation**

#### *Initial Try: Automated Segmentation*

Significant effort was dedicated to processing the data for inputs into the CNN. Initially, an automated segmentation algorithm was utilized to extract kidney segments from the complete biopsy slide (on the order of 150000x50000x3 pixels). The algorithm used standard image processing and segmentation techniques such as histogram thresholding, erosion, and dilation to mask the kidney cells from noise and background. Unfortunately, multiple attempts of training a CNN were unsuccessful with these inputs. 

#### *Semi-Automatic Segmentation*

Prior works have demonstrated a correlation between kidney function and the kidney cortex (see figure below). The automatic segmentation algorithm extracted entire kidney segments that contained significant portions of kidney medulla (see figure below). In order to focus on the primary goal of attempting to extract information from the kidney cortex a semi-automatic algorithm was developed. Possible future work would include automatic segmentation of kidney cortex from medulla.


![http://openslide.org/demo/ Aperio: CMU-1-JP2K-33005](images/cortex_v_medulla.PNG)

Thus, to prove that the kidney cortex is significant (as both an indicator of kidney function and training a CNN), our initial approach was to manually extract Regions of Interest (ROIs). These ROIs cropped views of the kidney biopsies to mostly contain the kidney cortex and glomeruli. In deployment, this would require a pathologist to manually extract ROIs from patients prior to feeding it to the CNN predictive pipeline. This extra step of ROI extraction took about 1 minute per patient’s slides.

Using Leica's ImageScope software (an interface capable of viewing, editing, and extracting ROIs from digital slides), a kidney database was generated containing segments of the kidney cortex over all the patients. There were on average 7 ROI extractions per slide, with resolutions ranging from (2000x2000x3) to (8000x8000x3) pixels.

After ROI extraction 3 challenges remained to be addressed: 1) The data was still sparse -- containing on average 35 ROI extractions per eGFR measurement; 2) ROI extraction resolution was much too large for a network to train with a reasonable batch size; and 3) ROI extractions had different resolutions and any common downsampling/upsampling to a common (height, width) would corrupt the physical shapes of the kidney cells. 

To address these challenges the ROIs were further processed into smaller image chips by cropping with a sliding window of size (2000x2000x3), overlap of 50% and then downsampling the crop by 2x. The resulting database 1) contained significantly more examples per patient; and 2) had manageable, uniform input resolutions  (1000x1000x3). An example of such an image chip can be seen below (not actual data).


![http://openslide.org/demo/ Aperio: CMU-1-JP2K-33005](images/example_slide_zoom.png)

In summary, the kidney biopsies collected from the Neptune study were cropped to selected views of each patient’s kidney biopsies. These “image chips” would then be fed into the CNN for training, with each image chip matched with the patient’s 12 month eGFR. Finally, the predictions of each image chip per patients are averaged for the final eGFR prediction.

Code Blurb: This database generation method was created using [gen_roi_db.py](Code/gen_roi_db.py). 

####  *Data Augmentation*
Upon loading the pre-processed database for training the CNN, the data is downsampled again by another 2x-4x (resulting in 500x500x3 to 250x250x3 images) and randomly augmented on-the-fly using the python package [datumio](https://github.com/longubu/datumio). 
The following affine transformations were selected based on realistic expectations of the data,. 

- **rotation:** random angle between -15&deg; and 15&deg;
- **translation**: random x,y translation of 7%
- **rescaling**: random scale (zooming) factor of 5%
- **flipping**: 50% left/right and up/down symmetrical flipping
- **cropping**: after all previous augmentation, center crops of size (400, 400, 3)


The resulting inputs of the CNN were randomly perturbed views of the kidney biopsies of size (400, 400, 3). Below is an example of random augmentations applied to an example kidney view (not the actual data, but similar augmentation parameters).

![http://openslide.org/demo/ Aperio: CMU-1-JP2K-33005](images/data_aug_example.png)

### **Network Architecture**

The convnet architecture used for KidneyCHLA was heavily inspired by VGGNet (OxfordNet) -- a very deep Convolutional Neural Network that yields small convolutional filters for construction of deeper layers. The CNN architecture used for preliminary results are shown [here](images/vgg16_kidneyv4_v3.png)

The network utilized by KidneyCHLA is shallower than VGG, but follows the technique of “same” convolutions (when output feature maps are the same size as the input feature maps) of 3x3 filters. This variation allows for feeding the network larger inputs and maintaining reasonable computational time. 

Another variation to the VGG network is that the KidneyCHLA network concatenates “aux-features” (hand-engineered features and any auxiliary information) into the dense layers. Inserting the features at the dense layer guides the network’s classification layers to not only leverage the learned compressed feature basis developed by the convolutions but also additional signals injected from *a prior* knowledge that the network could not extract from the kidney biopsy images.

Future works include implementing more recently successful techniques and layers such as [Batch Normalization](http://arxiv.org/abs/1502.03167) and [Residual Networks](http://arxiv.org/abs/1512.03385).

Code Blurb: Models were created in python scripts in the [Models](Models/) directory. The preliminary network is constructed using [vgg_kidneyv4_v3.py](Models/vgg_kidneyv4_v3.py).

#### *Inputs*

Inputs to the network were [image chips](#pre-processing-and-data-augmentation) of each patient’s kidney slides and their associated aux-features. The images were passed to the convolutional layers while the aux-features were fed to the one of the dense layer. For the preliminary network, no hand-engineered features were created yet and the only aux-features appended were initial eGFR measurements. The addition of the initial eGFR alone improved the network greatly -- decreasing the training time by 2x and the validation error of the network by 20%. 

### **Training**

#### *Performance Metric & Validation*

For the network to be useful, it should be able to predict the eGFR of an entirely new patient. This means that the network should be transparent to never-before-seen biopsy slides, color-dyes, and digital imaging techniques. Thus, to properly evaluate performance based on these criterion, the training/validation sets were split based on labels of unique patients. This enforces that any image chip (slice of kidney biopsy slide) associated with a patient cannot be included in both the training and validation set. This restriction aligns with our criterion in that the validation error represents the confidence of the network’s ability to extrapolate a never-before-seen patient’s eGFR 12 months in the future.

Moreover, due to the small amount truth (a little over 80 unique patients, even less with sufficient measurements and follow ups), a simple train/test split of 80/20 would only leave the validation with less than 16 patients. Thus, 5-fold patient-level cross-validations were also used for more thorough investigations of network performance.

##### *Additional Metrics*

An additional metric used to evaluate model performance is a scatter plot of the true eGFR values (x) vs the predicted eGFR values (y). This was not a loss function used for optimizing the network, but served as an intuitive performance metric that is much easier to understand than a single number such as mean-squared-error. Qualitatively, the models can be compared based on how close the points are to a 1-to-1 line; Quantitatively, the models can be compared based on a the residuals of a least-squared linear fit to the predicted eGFR compared to the 1-to-1 line.

#### *Optimizer and Hyper-Parameters*

The models were trained using RmsProp, an adaptive learning rate that divides the current gradient by the moving average over the root-mean-squared (hence the rms) of the weighted sum of the recent gradients. RMSProp can be seen as an extension of Adagrad with the addition of momentum. Hyper-parameters of RmsProp were left to their default values: `rho=0.9` and `epsilon=1e-06` with an initial learning rate of `lr=1-e4`.

The learning rate was linearly decreased after every epoch (an entire loop through the training set). Weight updates were performed after every batch size of 32 and typically learned for 20-30 epochs. Using a NVIDIA Titan X, the network took on average 7 minutes to run through an epoch.

Future plans include investigations of other optimizers such as ADAM, hyper-parameter searches, and better learning rate procedures.

#### *Initialization*

All layers of the network (with weights) were initialized using Glorot uniform (Xavier initialization) which scales the weight elements to the number of parameters of input and the output of the layer. More specifically, each element of a layer’s weights draws from a uniform distribution with zero bias in the interval W ~ U(-sqrt( 6/sqrt(n_in + n_out) ), sqrt( 6/(n_in + n_out) )) (this is kera’s [implementation](https://github.com/fchollet/keras/blob/master/keras/initializations.py)), where n_in is the number of parameters feeding into the layer and n_out is the number of parameters outputted from the layer. 

### **Results**

The preliminary network’s mean absolute error of predicting 12 months eGFR is 17.55. As a comparison, a model that “predicts” 12 months eGFR using all of the patients’ averages has an absolute error of 30.5. This is a 42% percent difference in model errors, illustrating that the network was able to learn useful features from patients’ kidney biopsies for predicting eGFR. <More Results… />