# -*- coding: utf-8 -*-
"""
Created on Fri Oct 30 21:31:07 2015

@author: damiauzo

Code to parse through the supplied truth file to generate our regression target
"""

import pandas as pd
import numpy as np
import os
from collections import defaultdict
from scipy.stats import linregress
import matplotlib.pyplot as plt


#DRL - path to the supplied truth information
truthPath    = r'/media/damiauzo/Projects/VPICU/Data/Kidney/Truth/Morph_eGFR_export15OCT2015.csv'
measuresPath = r'/media/damiauzo/Projects/VPICU/Data/Kidney/Truth/SUMMARY_DATA.csv'

#DRL - path to write out the pre-processed truth
outPath = r'/media/damiauzo/Projects/VPICU/Data/Kidney/Truth/'

#DRL - if the outPath folder doesn't exist create it
if not os.path.exists(outPath):
    os.makedirs(outPath)

#DRL - read in the raw truth data
dataDF = pd.read_csv(truthPath)
measDF = pd.read_csv(measuresPath)

#DRL - simplify the VisitNum convention
convertDict = defaultdict(lambda: np.NaN)
convertDict.update({'1: V01- Eligibility Assessment': 'EligibilityAssessment',
                    '2: V02- Baseline': 'Baseline',
                    '3: V03- Biopsy': 'Biopsy',
                    '4: V04- 4 Month Follow Up': '4_month',
                    '5: V05- 8 Month Follow Up': '8_month',
                    '6: V06- 1 Year Follow Up': '12_month',
                    '7: V07- 18 Month Follow Up': '18_month',
                    '8: V08- 2 Year Follow Up': '24_month',
                    '9: V09- 30 Month Follow Up': '30_month',
                    '10: V10- 36 Month Follow Up': '36_month',
                    '11: V11- 42 Month Follow Up': '42_month',
                    '12': '48_month',
                    '13': '54_month' })

dataDF['visitTime'] = dataDF['VisitNum'].apply(lambda x: convertDict[x])

#DRL - create a dataframe to hold the truth columns of interest
truthDF = dataDF.pivot('patientstudyID', 'visitTime', 'eGFR')

#DRL - remove the columns for biopsy and eligibibility assessment
del truthDF['EligibilityAssessment']
del truthDF['Biopsy']

#DRL - reorder the columns for convenience
truthDF = truthDF[['Baseline', '4_month', '8_month', '12_month', 
                   '18_month', '24_month', '30_month', '36_month', 
                   '42_month', '48_month', '54_month']]

#DRL - create a lienar regression to each patient to get their eGFR slope
slope_eGFR  = dataDF.dropna().groupby('patientstudyID').apply(lambda x: linregress(x['VisitStuDt'], x['eGFR'])[0]).dropna()
bias_eGFR  = dataDF.dropna().groupby('patientstudyID').apply(lambda x: linregress(x['VisitStuDt'], x['eGFR'])[1]).dropna()

truthDF['slope_eGFR'] = slope_eGFR
truthDF['bias_eGFR']  = bias_eGFR

#LVH - use linear fit to regress to 12,24,36 eGFR
for month in [12,24,36]:
    truthDF['%s_month_regress'%month] = truthDF['slope_eGFR'] * month + truthDF['bias_eGFR']

#DRL - create a convenience key to map with the measurement data
truthDF = truthDF.reset_index()
truthDF['CASE #'] = truthDF['patientstudyID'].apply(lambda x: '-'.join(x.split('-')[-2:]))

#DRL - join the measurement columns of interest to the truth
columns = ['CASE #', 'PAS - INTERSTITIUM', 'PAS - VESSEL', 'PAS - INTACT TUBULE',
           'PAS - ATROPHIC TUBULE', 'PAS - PATENT GLOM', 'PAS - SCLEROTIC GLOM']
truthDF = pd.merge(truthDF, measDF[columns], how='left', on='CASE #')

#DRL - write out the truth
truthDF.reset_index().to_csv(os.path.join(outPath, 'preProcessed_eGFR_truth_wRegress_wMeasures_wBaseline.csv'), index=False)

#plt.figure()
##DRL - test the eGFR slopes
#for patient in slope_eGFR.index:
#    #plot the raw points
#    plt.clf()
#    tmp = dataDF[dataDF['patientstudyID'] == patient].copy()
#    plt.scatter(tmp['VisitStuDt'], tmp['eGFR'], alpha=.5, label='measurements')
#    plt.plot(tmp['VisitStuDt'], (tmp['VisitStuDt'] * slope_eGFR[patient] + bias_eGFR[patient]), label='linRegression')
#    plt.title(patient)
#    plt.xlabel('Days since Baseline')
#    plt.ylabel('eGFR')
#    plt.legend()
#    plt.tight_layout()
#    plt.savefig('%s_eGFR.png' % patient)
