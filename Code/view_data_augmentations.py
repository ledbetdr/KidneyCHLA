"""
Generates batches similar to kidney_driver to view augmentations.
Mimics kidney_driver. Change as it does.

Intersting cases:
['10_26609_001_001 L7 TRI_01_001.npy','10_26609_001_001 L7 TRI_01_002.npy',
'10_26609_001_001 L7 TRI_01_003.npy', '10_26609_001_001 L7 TRI_01_004.npy']
"""
from __future__ import absolute_import

from datumio import datagen as dtd
import numpy as np
import matplotlib.pyplot as plt
import os
import pandas as pd
from sklearn.cross_validation import LabelKFold
import shutil
from utils import dataUtils, argUtils


plt.ioff()  # no interactive plotting


def plot_og_n_augment(img, img_og, fignum=1):
    """Plot original image alongside augmented image"""
    plt.figure(fignum)
    plt.clf()
    fig, axes = plt.subplots(1, 2, num=fignum, figsize=(14, 14))
    ax = axes[0]
    ax.imshow(img_og)
    ax.set_title("Original Image")
    ax = axes[1]
    ax.imshow(img)
    ax.set_title("Augmented Image")
    plt.tight_layout()
    return ax

# parse command line arguments
args = argUtils.argparser()
run_name = args['run_name']
model_name = args['model']
target = args['target']

# set up output paths based on run_name
base_output = args['output_dir']
output_dir = os.path.join(base_output, run_name)
if not os.path.exists(output_dir):
    os.mkdir(output_dir)

# save this script and arg info to output_dir
shutil.copy2(__file__, os.path.join(output_dir, os.path.basename(__file__)))

# set up output paths for augmentations
train_out_dir = os.path.join(output_dir, 'train_augs')
if not os.path.exists(train_out_dir):
    os.mkdir(train_out_dir)
valid_out_dir = os.path.join(output_dir, 'valid_augs')
if not os.path.exists(valid_out_dir):
    os.mkdir(valid_out_dir)
rng_eg_out_dir = os.path.join(output_dir, 'rng_eg_aug')
if not os.path.exists(rng_eg_out_dir):
    os.mkdir(rng_eg_out_dir)

# ----- set up database info ----- #

db = args['database']
data_dir = os.path.join(db, 'npy')
truth_df = pd.read_csv(os.path.join(db, 'truth_w_RegressMeasureBaseline.csv'))
prev_len = len(truth_df)
truth_df.dropna(subset=[target], inplace=True)  # drop data with no truth
print('... Kept %i out of %i data chips due to lack of truth'
      % (len(truth_df), prev_len))

# set up inputs for datumio
truth_df['datapath'] = truth_df['chip_uid'].apply(
                         lambda x: os.path.join(data_dir, x + '.npy')).values

# split data into train/valid
cv_gen = LabelKFold(truth_df['patientstudyID'], n_folds=2)

# ----- Cross-validation training ----- #

X = truth_df['datapath'].values
Y = truth_df[target].values

for it, (train_idx, valid_idx) in enumerate(cv_gen):
    # select only a small subset to plot
    train_idx = np.random.choice(train_idx, size=500, replace=False)
    valid_idx = np.random.choice(valid_idx, size=500, replace=False)
    X_train, Y_train = X[train_idx], Y[train_idx]
    X_valid, Y_valid = X[valid_idx], Y[valid_idx]

    # set on-the-fly data augmentation parameters
    ds_shape = args['ds_shape']  # downsample shape
    input_shape = args['input_shape']  # final cropped shape from datumio
    rng_aug_params = {
        'zoom_range'        : (1/args['zoom'], args['zoom']),
        'rotation_range'    : (-args['rotation'], args['rotation']),
        'shear_range'       : (-args['shear'], args['shear']),
        'translation_range' : (-args['translation'], args['translation']),
        'do_flip_lr'        : True,
        'do_flip_ud'        : True,
        'allow_stretch'     : False,
        'output_shape'      : tuple(input_shape),
        'warp_kwargs'       : {'mode': 'constant', 'cval': 0.0}
    }
    static_aug_params = {'output_shape': tuple(input_shape)}

    # set up data generator
    BATCH_SIZE = args['batchsize']

    traingen = dtd.DataGenerator(
                  X_train, y=Y_train, data_loader=dataUtils.load_chip,
                  dl_kwargs={'normalize': True, 'ds_shape': ds_shape},
                  rng_aug_params=rng_aug_params)

    validgen = dtd.DataGenerator(
                  X_valid, y=Y_valid, data_loader=dataUtils.load_chip,
                  dl_kwargs={'normalize': True, 'ds_shape': ds_shape},
                  aug_params=static_aug_params)

    # set up data generator for the originals
    traingen_og = dtd.DataGenerator(
                     X_train, y=Y_train, data_loader=dataUtils.load_chip,
                     dl_kwargs={'normalize': True, 'ds_shape': ds_shape},
                     aug_params=static_aug_params)

    validgen_og = dtd.DataGenerator(
                     X_valid, y=Y_valid, data_loader=dataUtils.load_chip,
                     dl_kwargs={'normalize': True, 'ds_shape': ds_shape},
                     aug_params=static_aug_params)

    # ----- iterate through batch and save ----- #

    print("Working on training data")
    ndata = len(X_train)
    nb_batch = int(np.ceil(float(ndata)/BATCH_SIZE))
    trainbatchgen = traingen.get_batch(buffer_size=3, chw_order=False,
                                       shuffle=False, batch_size=BATCH_SIZE)
    trainbatchgen_og = traingen_og.get_batch(buffer_size=3, chw_order=False,
                                             shuffle=False,
                                             batch_size=BATCH_SIZE)

    for idx, (X_batch, Y_batch) in enumerate(trainbatchgen):
        print('[Train] On batch %i out of %i' % (idx+1, nb_batch))

        X_batch_og, Y_batch_og = trainbatchgen_og.next()
        datapaths = X_train[idx*BATCH_SIZE:(idx+1)*BATCH_SIZE]

        if not np.all(Y_batch == Y_batch_og):
            raise RuntimeError("Training batches are not equal")

        for it, datapath in enumerate(datapaths):
            ax = plot_og_n_augment(X_batch[it], X_batch_og[it])
            sfp = os.path.basename(datapath).split('.')[0] + '.png'
            plt.savefig(os.path.join(train_out_dir, sfp), bbox_inches='tight')

    print("Working on validation data")
    ndata = len(X_valid)
    nb_batch = int(np.ceil(float(ndata)/BATCH_SIZE))
    validbatchgen = validgen.get_batch(buffer_size=3, chw_order=False,
                                       shuffle=False, batch_size=BATCH_SIZE)
    validbatchgen_og = validgen_og.get_batch(buffer_size=3, chw_order=False,
                                             shuffle=False,
                                             batch_size=BATCH_SIZE)

    for idx, (X_batch, Y_batch) in enumerate(validbatchgen):
        print('[Valid] On batch %i out of %i' % (idx+1, nb_batch))

        X_batch_og, Y_batch_og = validbatchgen_og.next()
        datapaths = X_valid[idx*BATCH_SIZE:(idx+1)*BATCH_SIZE]

        if not np.all(Y_batch == Y_batch_og):
            raise RuntimeError("Training batches are not equal")

        for it, datapath in enumerate(datapaths):
            ax = plot_og_n_augment(X_batch[it], X_batch_og[it])
            sfp = os.path.basename(datapath).split('.')[0] + '.png'
            plt.savefig(os.path.join(valid_out_dir, sfp), bbox_inches='tight')

    # populate some examples from og and random augmentations
    print("Populating small subset of e.g with multiple augmentations views")
    n_augs = 8
    X_rng = np.random.choice(X_train, size=5, replace=False)
    X_rng = np.array(list(X_rng) * n_augs)

    traingen = dtd.DataGenerator(
                  X_rng, data_loader=dataUtils.load_chip,
                  dl_kwargs={'normalize': True, 'ds_shape': ds_shape},
                  rng_aug_params=rng_aug_params)
    trainbatchgen = traingen.get_batch(buffer_size=2, chw_order=False,
                                       shuffle=False, batch_size=BATCH_SIZE)

    # set up data generator for the originals
    traingen_og = dtd.DataGenerator(
                     X_rng, data_loader=dataUtils.load_chip,
                     dl_kwargs={'normalize': True, 'ds_shape': ds_shape},
                     aug_params=static_aug_params)

    trainbatchgen_og = traingen_og.get_batch(buffer_size=2, chw_order=False,
                                             shuffle=False,
                                             batch_size=BATCH_SIZE)

    for idx, X_batch in enumerate(trainbatchgen):
        X_batch_og = trainbatchgen_og.next()
        datapaths = X_rng[idx*BATCH_SIZE:(idx+1)*BATCH_SIZE]

        for it, datapath in enumerate(datapaths):
            ax = plot_og_n_augment(X_batch[it], X_batch_og[it])
            sfp = os.path.basename(datapath).split('.')[0] + '%02d.png' % it
            plt.savefig(os.path.join(rng_eg_out_dir, sfp), bbox_inches='tight')

    break  # don't perform cv
