"""
Generate database for Kidney CNN using manually extracted ROIs from slides.
"""
import glob
import numpy as np
import os
import pandas as pd
from PIL import Image
import shutil
from skimage.measure import block_reduce

# local imports
from utils import windowSliceUtils as wsu
from utils import dataUtils as du


def get_roi_chips(roi_img, chip_size=(1000, 1000, 3), overlap=0.5):
    """From roi_img, extracts image chips of chip_size with window overlap"""
    chip_h, chip_w, chip_c = chip_size

    # check height/width to be > chip sizes
    width, height = roi_img.size
    if height < chip_h or width < chip_w:
        roi_chips = None
    else:
        stride_h = int(chip_h*(1.0-overlap))
        stride_w = int(chip_w*(1.0-overlap))

        roi_chips = wsu.sliding_window(np.array(roi_img),
                                       (chip_h, chip_w, chip_c),
                                       (stride_h, stride_w, 0))

        # Check for cases where only 1 window fits entire image.
        # In these cases, we must reshape so we can loop through as a list
        if len(roi_chips.shape) == 3:
            roi_chips = np.expand_dims(roi_chips, 0)

    return roi_chips


def gen_db(chip_shape, overlap=0.80, ds=2):
    print("Generating %s of size (%i,%i,%i) and overlap %0.2f"
          % (dbName, chip_h, chip_w, chip_c, overlap))

    chip_h, chip_w, chip_c = chip_shape
    dbName = 'roi_dbv1_%ix%ix%i_%dperc_overlap_%dxdownsample'
             % (chip_h, chip_w, chip_c, np.int(100 * overlap), ds)

    roiDir      = '/media/damiauzo/Projects/VPICU/Data/Kidney/Extracted'
    roiMetaPath = '/media/damiauzo/Projects/VPICU/Data/Kidney/Truth/slide_roi.csv'
    truthPath   = '/media/damiauzo/Projects/VPICU/Data/Kidney/Truth/preProcessed_eGFR_truth_wRegress.csv'

    # Set up output paths
    baseOutDir      = '/media/damiauzo/Projects/VPICU/Data/Kidney/'
    dbOutDir        = pl.check_make_dir(os.path.join(baseOutDir, dbName))
    pngDir          = pl.check_make_dir(os.path.join(dbOutDir, 'png'))
    npyDir          = pl.check_make_dir(os.path.join(dbOutDir, 'npy'))
    dbTruthOutpath  = os.path.join(dbOutDir, 'truth.csv')

    # Get slides with valid extractions
    roiDF       = pd.read_csv(roiMetaPath)
    roiDF       = roiDF[roiDF['roi_status'] == 1]
    slidePaths  = roiDF['slidepath'].values

    # Load truth dataframe and set up output to store chip truths
    truthDF = pd.read_csv(truthPath)
    truthDF.set_index("patientstudyID", inplace=True)

    # Iterate through each slide, get ROIs and create window chips
    nSlides = len(slidePaths)
    for siter, slidePath in enumerate(slidePaths):
        print('... Working on %i out of %i' % (siter+1, nSlides))
        patientstudyID = du.get_patientID_from_datapath(slidePath)

        roiPaths = glob.glob(os.path.join(roiDir, '%s*.tif'
                             % '.'.join(slidePath.split('.')[:-1])))

        chip_uids = []
        for roiPath in roiPaths:
            roi_img = Image.open(roiPath, mode='r')
            roi_chips = get_roi_chips(roi_img,
                                      chip_size=(chip_h, chip_w, chip_c),
                                      overlap=overlap)

            if roi_chips is None:
                print("... ... Skipping %s because height/width is smaller than chip size" % (roiPath))
                continue

            for witer, roi_chip in enumerate(roi_chips):
                chip_uid = '.'.join(os.path.basename(roiPath).split('.')[:-1])
                chip_uid += '_%03d' % (witer + 1)
                chip_uids.append(chip_uid)

                # DRL - downsample
                if ds > 0:
                    roi_chip = block_reduce(roi_chip,
                                            (2 * ds, 2 * ds, 1),
                                            func=np.mean)
                    roi_chip = np.round(roi_chip)
                    roi_chip = roi_chip.astype(np.uint8)

                # save chip to npy and png directories
                Image.fromarray(roi_chip).save(os.path.join(pngDir,
                                                            chip_uid + '.png'))
                np.save(os.path.join(npyDir, chip_uid + '.npy'), roi_chip)

        # store chip with corresponding truth
        chipsDF = pd.DataFrame(chip_uids, columns=['chip_uid'])
        chipsDF['patientstudyID'] = patientstudyID
        for col in truthDF.columns:
            try:
                chipsDF[col] = truthDF.loc[patientstudyID, col]
            except KeyError:
                chipsDF[col] = np.NaN  # patienstudyID not located in truthDF

        # save chip info with truth
        if siter == 0:
            chipsDF.to_csv(dbTruthOutpath, index=False)
        else:
            with open(dbTruthOutpath, 'a') as fout:
                chipsDF.to_csv(fout, index=False, header=False)

    # copy script file to db output path for reproducibility
    shutil.copy2(__file__, os.path.join(dbOutDir, os.path.basename(__file__)))

if __name__ == "__main__":
    import argparse
    from utils import argUtils

    # set up inputs defaults and cmd line arg parser
    p = argparse.ArgumentParser()
    p.add_argument('--chip_shape',    type=argUtils.int_list_parser,    default='2000,2000,3')
    p.add_argument('--overlap',       type=float,                       default=0.8)
    p.add_argument('--ds',            type=float,                       default=2)

    # convert into a dictionary
    args = vars(p.parse_args())

    # generate database
    gen_db(args['chip_shape'], overlap=args['overlap'],
           ds=args['ds'])
