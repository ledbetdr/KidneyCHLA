#!/usr/bin/python2.7
"""
Train a deep CNN on Kidney Biospy slides to predict eGFR.
"""
from __future__ import print_function
from datumio import datagen as dtd
import keras
from keras.utils import layer_utils
import numpy as np
import os
import pandas as pd
from sklearn.cross_validation import LabelKFold, LabelShuffleSplit
import shutil
import yaml
from utils import dataUtils, kerasUtils, plotUtils, argUtils  # local imports


# Parse command line arguments
args = argUtils.argparser()
run_name = args['run_name']
model_name = args['model']
target = args['target']

# print parsed information
intro_text = " Run: %s | Model: %s " % (run_name, model_name)
print("="*15, intro_text, "="*15)
for key, item in args.iteritems():
    print("=", "{: >20}: {: >40}".format(key, item))
print("="*(30 + len(intro_text)))

# set up output paths based on run_name
base_output = args['output_dir']
output_dir = os.path.join(base_output, run_name)
if not os.path.exists(output_dir):
    os.mkdir(output_dir)

plot_dir = os.path.join(output_dir, 'plots')  # dir to store plots
if not os.path.exists(plot_dir):
    os.mkdir(plot_dir)

# save this script and arg info to output_dir
shutil.copy2(__file__, os.path.join(output_dir, os.path.basename(__file__)))
with open(os.path.join(output_dir, 'run_info.yml'), 'w') as fout:
    fout.write(yaml.dump(args))

# ----- set up database info ----- #

db = args['database']
data_dir = os.path.join(db, 'npy')
truth_df = pd.read_csv(os.path.join(db, 'truth_w_RegressMeasureBaseline.csv'))
prev_len = len(truth_df)
truth_df.dropna(subset=[target], inplace=True)  # drop data with no truth
truth_df.dropna(subset=['Baseline'], inplace=True)  # those with no baseline
print('... Kept %i out of %i data chips due to lack of truth'
      % (len(truth_df), prev_len))

# set up inputs for datumio and storing predictions
truth_df['datapath'] = truth_df['chip_uid'].apply(
                         lambda x: os.path.join(data_dir, x + '.npy')).values

# new col to distinguish between truth & pred
pred_col = [t + '_pred' for t in target]
truth_df = truth_df.reindex(columns=list(truth_df.columns) + pred_col)
truth_df = truth_df.drop_duplicates('datapath').set_index('datapath')
pred_df = truth_df.copy()  # will store agg cv predictions
pred_df['fold'] = np.NaN  # record cv-fold per item

# ----- Cross-validation training ----- #
n_folds = args['n_folds']
if n_folds == 1:
    cv_gen = LabelShuffleSplit(truth_df['patientstudyID'], n_iter=1,
                               test_size=0.2, random_state=args['rng_seed'])
else:
    cv_gen = LabelKFold(truth_df['patientstudyID'], n_folds=args['n_folds'])
"""cv_gen splits the data on a Patient-level. Each patient contains multiple
slides of kidney biopsies, each slide is further segmented to contain
only kidney cells (in particular glomeruli). Consequently, the cross-
validation folds are split such that all the kidney images of a patient can
only appear in the training OR the test set, never in both. This gives us
accurate measure of performance on our model predictions on new patients"""

X = truth_df.index.values
Y = truth_df[target].values
Y *= args['multiply_truth']
for it, (train_idx, valid_idx) in enumerate(cv_gen):
    X_train, Y_train = X[train_idx], Y[train_idx]
    X_valid, Y_valid = X[valid_idx], Y[valid_idx]

    pred_df['fold'].iloc[valid_idx] = it + 1  # store cv-fold of current iter
    cv_pred = truth_df.iloc[valid_idx].copy()  # to temporarily store preds

    # set up paths for network-monitor, depending on cmd-line args
    if args['save_errors_path'] == 'default':
        errors_path = os.path.join(output_dir, 'cv_%02d_errors.csv' % (it + 1))
    else:
        errors_path = args['save_errors_path']

    if args['save_weights'] == 'default':
        weights_path = os.path.join(output_dir,
                                    'best_valid_params_cv_%02d.h5' % (it + 1))
    else:
        weights_path = args['save_weights']

    # set on-the-fly data augmentation parameters
    ds_shape = args['ds_shape']  # downsample shape
    input_shape = args['input_shape']  # final cropped shape from datumio
    rng_aug_params = {
        'zoom_range'        : (1/args['zoom'], args['zoom']),
        'rotation_range'    : (-args['rotation'], args['rotation']),
        'shear_range'       : (-args['shear'], args['shear']),
        'translation_range' : (-args['translation'], args['translation']),
        'do_flip_lr'        : True,
        'do_flip_ud'        : True,
        'allow_stretch'     : False,
        'output_shape'      : tuple(input_shape),
        'warp_kwargs'       : {'mode': 'constant', 'cval': 0.0}
    }
    static_aug_params = {'output_shape': tuple(input_shape)}

    # set up data generator for train data & validation data using datumio
    traingen = dtd.DataGenerator(
                  X_train, y=Y_train, data_loader=dataUtils.load_chip,
                  dl_kwargs={'normalize': True, 'ds_shape': ds_shape},
                  rng_aug_params=rng_aug_params, dataset_zmuv=True,
                  dataset_axis=(0, 1))  # channel-wise zmuv

    validgen = dtd.DataGenerator(
                  X_valid, y=Y_valid, data_loader=dataUtils.load_chip,
                  dl_kwargs={'normalize': True, 'ds_shape': ds_shape},
                  aug_params=static_aug_params)
    validgen.mean = traingen.mean
    validgen.std = traingen.std

    # set up model
    print('... Building & compiling model')  # this might take a while...
    nb_epoch = args['nb_epoch']
    BATCH_SIZE = args['batchsize']
    save_every_epoch = args['save_every_epoch']
    lr = args['start_lr']
    stop_lr = args['stop_lr']
    if stop_lr > lr:
        raise RuntimeError("stop_lr (%s) > start_lr (%s)" % (stop_lr, lr))

    # build model -- based on name of model provided from cmd-line
    model_args = [tuple([ds_shape[2]] + input_shape), 1, len(target)]
    model_kwargs = {'weights_path': args['load_weights']}
    model = kerasUtils.get_network_model(
               model_name, model_args, model_kwargs=model_kwargs)

    # print model if first cv iteration
    if it == 0:
        layer_utils.model_summary(model)

    # get optimizer by name
    opt = kerasUtils.get_optimizer(args['optimizer'], lr=lr)
    model.compile(loss={'output': 'mse'}, optimizer=opt)

    # set network monitor: records errors (train/valid), saves weights, etc.
    nm = kerasUtils.NetworkMonitor(
            weights_path, errors_path, patience=args['patience'],
            save_every_epoch=save_every_epoch)

    # set up learning rate scheduler
    # TODO(Long): A more intelligent learning rate schedule
    lr_scheduler = kerasUtils.LrReducer(patience=args['patience']/3)

    # start epoch loop
    for e in range(nb_epoch):
        print('-'*40)
        print('Epoch: %i/%i | CV: %i/%i'
              % (e + 1, nb_epoch, it + 1, n_folds))
        print('-'*40)

        if args['train_model']:
            print('Training Model')
            # seed with known random shuffle
            rng_seed = np.random.randint(0, 10e6)
            idxs = range(len(X_train))
            np.random.RandomState(rng_seed).shuffle(idxs)

            train_loss = []
            progbar = keras.utils.generic_utils.Progbar(len(X_train))
            batch_gen = traingen.get_batch(batch_size=BATCH_SIZE, shuffle=True,
                                           rng_seed=rng_seed, buffer_size=3,
                                           chw_order=True, dtype=np.float32)

            for idx, (X_batch, Y_batch) in enumerate(batch_gen):
                # get hand-crafted features to append to model
                batch_slice = idxs[len(X_batch)*idx: (idx+1)*len(X_batch)]
                X_features = truth_df.loc[X_train[batch_slice],
                                          'Baseline'].values.reshape((-1, 1))

                # compute loss
                loss = model.train_on_batch(
                          {'data': X_batch, 'features': X_features,  # inputs
                           'output': Y_batch})[0]                    # outputs

                train_loss.append(float(loss))
                progbar.add(X_batch.shape[0], values=[("train loss", loss)])
            avg_train_loss = np.mean(train_loss)
            print  # print empty line after training b/c progbar doesn't
        else:
            avg_train_loss = np.NaN

        if args['predict_on_valid']:
            print("Predicting on validation set")
            valid_loss = []
            progbar = keras.utils.generic_utils.Progbar(len(X_valid))
            batch_gen = validgen.get_batch(batch_size=BATCH_SIZE,
                                           buffer_size=3, chw_order=True)

            for idx, (X_batch, Y_batch) in enumerate(batch_gen):
                # get hand-crafted features to append to model
                batch_slice = slice(len(X_batch)*idx, (idx+1)*len(X_batch))
                X_features = truth_df.loc[X_valid[batch_slice],
                                          'Baseline'].values.reshape((-1, 1))

                # pred and store
                pred = model.predict_on_batch(
                          {'data': X_batch, 'features': X_features})['output']

                tmp_df = pd.DataFrame(pred*(1.0/args['multiply_truth']),
                                      index=X_valid[batch_slice],
                                      columns=[pred_col])
                cv_pred.loc[X_valid[batch_slice], pred_col] = tmp_df.values

                # compute error
                mse = np.mean((pred - Y_valid[batch_slice])**2)
                valid_loss.append(mse)
                progbar.add(X_batch.shape[0], values=[('valid loss', mse)])

            avg_valid_loss = np.mean(valid_loss)
        else:
            avg_valid_loss = np.NaN  # set value, even if pred_valid = False

        # compute and store metrics between epochs
        is_best_valid = nm.update(e, model, avg_train_loss, avg_valid_loss)
        lr_scheduler.check_update(model, avg_valid_loss)

        if is_best_valid:
            # store this cv's best pred into our agg cv prediction dataframe
            pred_df.loc[cv_pred.index, pred_col] = cv_pred[pred_col].values

        # plot truth vs pred of validation data as model continues to train
        if (e > 0) and args['predict_on_valid'] and \
                ((e % args['plot_every_epoch'] == 0) or (is_best_valid)):
            for tgt, prd in zip(target, pred_col):
                print("Plotting regression of %s eGFR vs predicted" % (tgt))
                sfp = os.path.join(plot_dir, '%s_cv%0d_e%06d.png'
                                             % (tgt, it+1, e+1))
                ax = plotUtils.plot_regression(
                        cv_pred.dropna(subset=[prd]), pred_col=prd,
                        truth_col=tgt, save_path=sfp)

        # stop training if valid error did not get better after patience epochs
        if nm.stop_training or lr_scheduler.stop_training:
            print("Epoch %05d: Early stopping due patience %i epochs"
                  % (e, args['patience']))
            break

        # end epoch loop

# record best predictions to csv & plot all folds best preds
pred_df.to_csv(os.path.join(output_dir, 'best_cv_predictions.csv'))
for tgt, prd in zip(target, pred_col):
    sfp = os.path.join(plot_dir, '%s_allcv.png' % tgt)
    ax = plotUtils.plot_regression(pred_df.dropna(subset=[prd]), pred_col=prd,
                                   truth_col=tgt, save_path=sfp)

print("="*40)
print("Successfully completed running %s" % __file__)
print("="*40)
