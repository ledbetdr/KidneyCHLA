"""
Utility functions for I/O of Kidney data
"""
import numpy as np
import os
from PIL import Image


def load_chip(datapath, ds_shape=None, normalize=False):
    """Loads a Kidney database image chip

    Parameters
    -------
    datapath: str
        Path to image chip (npy)

    ds_shape: iterable of shape 2, default=None
        Shape to downsample to: (height, width). If None, no downsampling
        will be done.

    normalize: bool, default=False
        If true, will divide by 255 to set intensity range to [0,1].

    Returns
    -------
    x: ndarray
        Loaded kidney image chip
    """
    x = np.load(datapath)

    # downsample image
    if ds_shape is not None and ds_shape[:2] != x.shape[:2]:
        x = Image.fromarray(x).resize((ds_shape[1], ds_shape[0]))
        x = np.array(x)

    if normalize:
        x = x / 255.

    return x


def get_patientID_from_datapath(datapath):
    """Gets patientstudyID from filepath. Lots of ugly tricks due to poor
    naming convention of raw data"""
    base_name = os.path.basename(datapath)

    patientID = '-'.join(base_name.replace(' ', '_').split('_')[:4])
    patientID = patientID.replace(',', '')
    patientID = '-'.join(patientID.split('-')[:2] +
                         ['%03d' % int(patientID.split('-')[2])] +
                         [patientID.split('-')[3]])

    return patientID
