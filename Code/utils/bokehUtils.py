"""
Utility function for working with bokeh
"""
from PIL import Image
import numpy as np

def convertToRGBA(img):
    """ Converts img to RGBA """
    try: 
        if img.mode != "RGBA":
            img = img.convert("RGBA")
    except AttributeError:
        try:
            img = Image.fromarray(img)
            if img.mode != "RGBA":
                img = img.convert("RGBA")
        except:
            print 'Error: Image is not of proper numpy arr (typically dtype = np.uint8. Error in converting arr to PIL.Image.'
            return None
    return np.array(img)
     
def bkp_preprocImg(img, flipud = True):
    """ Preprocess a image arr (numpy or PIL.Image) to be absorbed bokeh's plotting."""
    #LVH - convert to RGBA if it's not, depending on whether numpy.arr or PIL.Image
    img = convertToRGBA(img)

    #LVH - traditional image conventions refer to top left corner as 0,0. 
    #    - however, bokeh has bottom left (0,0).
    flippedImg = np.flipud(img)
    
    #LVH - convert img to be 2d arr (M x N) with dtype = uint32 -- for bokeh
    bimg = np.squeeze(flippedImg.astype(np.uint8).view(np.uint32)) 
    return bimg
