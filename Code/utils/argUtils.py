"""
Parse arguments for KidneyCHLA's run scripts
"""
import argparse


class ArgParseError(Exception):
    """Raise error if error in parsing arguments"""
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return repr(self.msg)


def int_list_parser(x):
    """ Parse comma-delimited string into a list of ints """
    if (x[0] == '(' and x[-1] == ')') or \
            (x[0] == '[' and x[-1] == ']'):
        x = x[1:-1]

    return [int(item) for item in x.split(',')]


def str_list_parser(x):
    """ Parse comma-delimited string into a list of ints """
    if (x[0] == '(' and x[-1] == ')') or \
            (x[0] == '[' and x[-1] == ']'):
        x = x[1:-1]

    return [str(item) for item in x.split(',')]


def bool_parser(x):
    """ Parse string for bool """
    arg = x.lower()
    if arg == 'true':
        return True
    elif arg == 'false':
        return False
    else:
        raise ArgParseError("bool_parser does not understand %s" % x)


def argparser():
    """Parse cmd-line arguments for running kidney_driver or view_data_augs"""
    # parse command line arguments
    p = argparse.ArgumentParser()
    p.add_argument('--run_name',           type=str,                default='12_month_newDB')
    p.add_argument('--model',              type=str,                default='vgg16_kidneyv4_v3')
    p.add_argument('--output_dir',         type=str,                default='../Output')
    p.add_argument('--target',             type=str_list_parser,    default='PAS - INTERSTITIUM,PAS - VESSEL,PAS - INTACT TUBULE,PAS - ATROPHIC TUBULE,PAS - PATENT GLOM,PAS - SCLEROTIC GLOM')
    p.add_argument('--n_folds',            type=int,                default=1)
    p.add_argument('--database',           type=str,                default='/media/damiauzo/Projects/VPICU/Data/Kidney/roi_dbv1_2000x2000x3_80perc_overlap_2xdownsample')
    p.add_argument('--rng_seed',           type=int,                default=17)
    p.add_argument('--ds_shape',           type=int_list_parser,    default='500, 500, 3')
    p.add_argument('--input_shape',        type=int_list_parser,    default='425, 425')
    p.add_argument('--multiply_truth',     type=float,              default=1.0)

    p.add_argument('--train_model',        type=bool_parser,        default=True)
    p.add_argument('--predict_on_valid',   type=bool_parser,        default=True)
    p.add_argument('--load_weights',       type=str,                default=None)
    p.add_argument('--save_weights',       type=str,                default='default')

    p.add_argument('--nb_epoch',           type=int,                default=250)
    p.add_argument('--batchsize',          type=int,                default=32)
    p.add_argument('--start_lr',           type=float,              default=0.00001)
    p.add_argument('--stop_lr',            type=float,              default=0.000001)
    p.add_argument('--optimizer',          type=str,                default='RMSprop()')
    p.add_argument('--weight_decay',       type=float,              default=1e-6)

    p.add_argument('--zoom',               type=float,              default=1.05)
    p.add_argument('--rotation',           type=float,              default=15.0)
    p.add_argument('--shear',              type=float,              default=0.0)
    p.add_argument('--translation',        type=int,                default=35)

    p.add_argument('--patience',           type=int,                default=15)
    p.add_argument('--save_errors_path',   type=str,                default='default')
    p.add_argument('--save_every_epoch',   type=int,                default=50)
    p.add_argument('--plot_every_epoch',   type=int,                default=10)

    # convert into a dictionary
    args = vars(p.parse_args())

    return args