"""
Collection of utility functions to help with Keras.
Most functions are particular to the workflow & structure of KidneyCHLA proj.
"""
import glob
import importlib
import keras.optimizers
import numpy as np
import os
import pandas as pd
import sys


class KerasUtilsError(Exception):
    """Raise error if there is an error in one of kerasUtils functions"""
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return repr(self.msg)


def get_network_model(model_name, model_args, model_dir='../Models',
                      model_kwargs=None):
    """Particular to KidneyCHLA. Looks into the Model's directory, finds the
    python file constructing the particular model and imports the model. The
    model files must follow a particular structure -- see one for example

    Parameters
    -------
    model_name: str
        Name of python file constructing the model (without the .py ext)

    model_args: list
        Required positional arguments sent to models

    model_dir: str, default="../Models"
        Base directory containing the `model_name` python file.

    model_kwargs: dict, default=None
        Keyword arguments to pass to `model_name`.

    Returns
    -------
    model: keras model
        Keras model to be used

    Raises
    -------
    KerasUtilsError
        Could not find `model_name` in `model_dir`
    """
    if model_kwargs is None:
        model_kwargs = {}

    # find all models within `model_dir`
    model_list = glob.glob(os.path.join(model_dir, '*.py'))
    model_list = [os.path.basename(m).split('.')[0] for m in model_list]

    # search for a match
    if model_name not in model_list:
        raise KerasUtilsError("Could not find %s in models directory %s"
                              % (model_name, model_dir))
    else:
        # dirty hack to import model
        sys.path.insert(0, model_dir)
        model = importlib.import_module(model_name)
    return model.generate_model(*model_args, **model_kwargs)


def get_optimizer(opt_str, lr=None):
    """Get keras optimizer based on name. This is because most of keras's get
    opt by name come with ridiculous default values.

    Parameters
    -------
    opt_str: str
        String of function call of a initializing a keras optimizer.
        E.g: opt = RMSprop()

    lr: float, default=None
        Learning rate to pass to opt_str. Will override if opt_str was init
        with lr; for example: opt_str = RMSprop(lr=9000), specifying lr=0.1
        would initialize a return optimizer: opt = RMSprop(lr=0.1)

    Returns
    -------
    opt: Keras optimizer
        Keras optimizer to be returned
    """
    # fill in the import module
    opt_str = 'keras.optimizers.' + opt_str

    # fill in learning rate if not supplied
    if lr is not None and opt_str.find('lr') < 0:
        idx = opt_str.find('(')
        opt_str = opt_str[:idx] + '(lr=%s,' % lr + opt_str[(idx+1):]
    try:
        # run string as if a function
        opt = eval(opt_str)
    except SyntaxError as error:
        raise KerasUtilsError("Could not get optimizer from '%s' with err: %s"
                              % (opt_str, error))

    return opt


class NetworkMonitor(object):
    """Monitors a Keras model's training.

    Parameters
    --------
    weights_savepath: str,
        Path to save weights after every improvement of validation error

    errors_savepath: str,
        Path to save train & validation loss to csv. Also saves learning rate.

    patience: int, default=None,
        Number of epochs with no improvement after which training will be
        stopped. If None, will not activate this feature.

    save_every_epoch: int, default=None
        Save weights after every epoch % save_ever_epoch == 0. If None,
        will not save weights (except the best valid ones). Weights saved
        by this method are saved to the directory of weights_savepath,
        with the name following: 'Epoch_%05d.h5' % (epoch+1)
    """
    def __init__(self, weights_savepath, errors_savepath, patience=None,
                 save_every_epoch=None):
        self.weights_savepath = weights_savepath
        self.errors_savepath = errors_savepath

        if patience is None:
            self.patience = np.inf
        else:
            self.patience = patience

        if save_every_epoch is not None:
            self.save_dir = os.path.dirname(self.weights_savepath)
            self.save_every_epoch = save_every_epoch

        # initialize default properties
        self.train_loss = []
        self.valid_loss = []
        self.valid_min_err = np.inf
        self.epoch = 0
        self.wait = 0
        self.stop_training = False

    def update(self, epoch, model, train_loss, valid_loss):
        """Updated NM with current model state

        Parameters
        -------
        epoch: int,
            Current epoch of model

        model: Keras (sequential) model
            Model to save, if appropriate conditions are satisified

        train_loss: float
            Average loss of model at current epoch on training data

        valid_loss: float
            Average loss of model at current epoch on validation data

        Returns
        -------
        is_best_valid: bool
            Current validation loss is the best score so far.
        """
        self.epoch = epoch
        self.train_loss.append(train_loss)
        self.valid_loss.append(valid_loss)

        # if valid_loss is NaN or inf, set to inf to avoid errors
        if not np.isfinite(valid_loss):
            valid_loss = np.inf

        # save weights if validation error decreased
        if (valid_loss < self.valid_min_err) and (self.epoch > 0):
            print("... Epoch %05d: Improved from %0.5f to %0.5f saving model to %s"
                  % (epoch, self.valid_min_err, valid_loss, self.weights_savepath))

            model.save_weights(self.weights_savepath, overwrite=True)
            self.valid_min_err = valid_loss
            is_best_valid = True
            self.wait = 0
        else:
            if self.wait >= self.patience:
                self.stop_training = True
            self.wait += 1
            is_best_valid = False

        if self.save_every_epoch is not None:
            if (self.epoch % self.save_every_epoch == 0) and (self.epoch) > 0:
                    print ("... Epoch %05d: Saving model for checkpoint")
                    save_path = os.path.join(self.save_dir,
                                             'Epoch_%05d.h5' % (self.epoch+1))
                    model.save_weights(save_path, overwrite=True)

        learning_rate = float(model.optimizer.lr.get_value())
        errors_df = pd.DataFrame([[epoch, train_loss, valid_loss, learning_rate]],
                                 columns=['epoch', 'train_loss', 'valid_loss', 'learning_rate'])

        # save errors. append if already exists
        if os.path.exists(self.errors_savepath):
            with open(self.errors_savepath, 'a') as fout:
                errors_df.to_csv(fout, index=False, header=False)
        else:
            errors_df.to_csv(self.errors_savepath, index=False)

        return is_best_valid


class LrReducer(object):
    """ """
    def __init__(self, patience=5, reduce_rate=0.1, reduce_nb=5, verbose=1):
        self.patience = patience
        self.reduce_rate = reduce_rate
        self.reduce_nb = reduce_nb
        self.verbose = verbose

        self.wait = 0
        self.best_error = np.inf
        self.current_reduce_nb = 0
        self.stop_training = False

    def check_update(self, model, valid_loss):
        if valid_loss < self.best_error:
            self.best_error = valid_loss
            self.wait = 0
        else:
            if self.wait >= self.patience:
                self.current_reduce_nb += 1
                if self.current_reduce_nb < self.reduce_nb:
                    self.wait = 0
                    lr = model.optimizer.lr.get_value()
                    model.optimizer.lr.set_value(np.array(lr*self.reduce_rate,
                                                          dtype=np.float32))
                    print("Setting learning to %0.3E"
                          % model.optimizer.lr.get_value())

                else:
                    self.stop_training = True
            self.wait += 1
