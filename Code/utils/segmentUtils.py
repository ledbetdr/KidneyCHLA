"""
Utility script summarizing the segmentation procedure for Kidney biopsy slides.
Specifically optimized for PAS dyes with slide_level = 6 => 64x downsampling.
"""

import numpy as np
from skimage import morphology as skm
from scipy import ndimage as ndi

def segmentKidney(slide):
    """ Segments Kidney blobs. Optimized for PAS dye @ 64x downsampling [slide level 6]
    
    Parameters 
    ------
    slide: openslide
        Openslide object of Kidney slice.
    
    Returns
    ------
    (image, finalMask): tuple of ndarrays
        image: is the original image read from slide @ LVL 6.
        finalMask: binary mask of segmentation blobs. 1 is Kidney cells, 0 is background
    """
    #LVH - get image from slide
    SLIDELVL = 6
    image = slide.read_region((0, 0), SLIDELVL, slide.level_dimensions[SLIDELVL])
    
    #LVH - convert to RGB
    width, height = image.size
    img = np.array(image.convert("RGB"))
    
    #LVH - get RGB thresholds
    imageMask = get_threshold_mask(img)
    
    #LVH - apply morphological operations
    morphMasks = get_64ds_morphology_mask(imageMask)
    
    return (image, morphMasks[-1][0])

def extractMaskBlobs(slide, mask, slidelvl):
    """ Gets segmented blob regions from image. Rotates into the standard x-y coordinate
    system and extracts the smallest possible rectangle
    
    Parameters
    ------
    slide:
    
    mask: ndarray
        2-D image array indiciating binary values of valid/nonvalid points.
    
    slidelvl:
    
    Returns
    ------
    zip(slicedBlobs, blobMasks): list of tuple
        slicedBlobs: list of ndarrays
            List of each blob regions detected in mask, rotated and extracted rectangles with minimum area
            
        blobMasks: list of ndarrays
            List of each slicedBlobs binary mask, indicating valid & non-valid pixels.
    """
    from skimage import measure as skmeasure
    
    #LVH - label image blobs uniquely
    label_image = skmeasure.label(mask)

    #LVH - assign a regionprop class to each label
    blobs = skmeasure.regionprops(label_image)
    
    #LVH - get the lvl 0 dimension cause we'll have to do some mapping later
    dimx0, dimy0 = slide.level_dimensions[0]
    dimxlvl, dimylvl = slide.level_dimensions[slidelvl]
    xmap = dimx0 / dimxlvl
    ymap = dimy0 / dimylvl
    
    #LVH - list to store each extracted bob
    slicedBlobs = []
    blobMasks = []
    
    #LVH - iterate through each uniquely labeled blob
    for blob in blobs:
        #LVH - store an empty array, illuminating only the blob of interest
        tmpMask = np.zeros(mask.shape, dtype = np.uint8)
        tmpMask[label_image == blob.label] = 1
        
        #lVH - find the angle we need to rotate major/minor axis to x & y
        #    - this is done so that we can easily find the "minimum" rectangle bb
        angle = np.rad2deg(-blob.orientation)
        
        #LVH - if angle is very large, blob is better to be oriented vertically
        if np.abs(angle) > 45: 
            angle = angle - np.sign(angle)*90
        
        #LVH - cut unrotated, image bounding box provided by regionprop for mask & image
        #    - this makes it easy to rotate image about center of cropped pictur.
        y0, x0, y1, x1 = blob.bbox
        width  = x1-x0
        height = y1-y0
        #LVH - read the subimage, mapping the x0,y0 to the level0 dimension first
        subimage = np.array(slide.read_region(( x0*xmap, y0*ymap), slidelvl, (width, height)))[:,:,:3]#image[y0:y1, x0:x1]
        blobmask = blob.image.astype(np.uint8)
        blobmask[blobmask > 0] = 255 # LVH - so that we can convert to PIL Image for rotation
        
        #LVH - rotate image: for now let's use PIL image function...
        rotateKwargs = dict(resample = 3, expand = 1)
        subimage = rotateImage(subimage, angle, rotateKwargs) # resample = 2 -> bilinear
        blobmask = rotateImage(blobmask, angle, rotateKwargs)
        
        #LVH - convert blob mask back into binary bool
        blobmask[blobmask <= 255/2] = 0
        blobmask[blobmask > 255/2] = 1
        blobmask = np.array(blobmask, dtype = bool)
        
        #LVH - use skimage regionprops again on binary bool to get a smaller bbox
        #    - in the new rotated coordinate system that aligns with the default x-y indexing.
        label2 = skmeasure.label(blobmask)
        blob2 = skmeasure.regionprops(label2)[0]
        
        y0, x0, y1, x1 = blob2.bbox
        slicedBlobs.append(subimage[y0:y1, x0:x1])
        blobMasks.append(blobmask[y0:y1, x0:x1])
        
    return zip(slicedBlobs, blobMasks)
    
#==============================================================================
# Segmentation Threshold functions
#==============================================================================
def apply_rgb_threshold(image, rThresh = (120, 180), gThresh = (120, 199), bThresh = (120, 190)):
    """ Thresholds image using RGB ranges and returns a mask 
    
    Parameters
    ------
    image: PIL.Image or ndarray
        Image to be thresholded.
    rThresh: tuple, list of int
        Thresholds image to only keep anything between rThresh[0] and rThresh[1]
    gThresh: tuple, list of int
        Thresholds image to only keep anything between gThresh[0] and gThresh[1]
    bThresh: tuple, list of int
        Thresholds image to only keep anything between bThresh[0] and bThresh[1]
    
    Returns
    ------
    rMask, gMask, bMask: tuple of binary masks
        binary masks of each channel rgb
    
    """
    #LVH - convert PIL image
    try:
        image = np.array(image.convert("RGB"))
    except:
        pass

    #LVH - get thresholds
    RED_THRESH_LO, RED_THRESH_HI = rThresh
    GREEN_THRESH_LO, GREEN_THRESH_HI = gThresh
    BLUE_THRESH_LO, BLUE_THRESH_HI = bThresh 
    
    #LVH - threshold image
    redMask = (image[:,:,0] >= RED_THRESH_LO) & (image[:,:,0] <= RED_THRESH_HI)
    greenMask = (image[:,:,1] >= GREEN_THRESH_LO) & (image[:,:,1] <= GREEN_THRESH_HI)
    blueMask = (image[:,:,2] >= BLUE_THRESH_LO) & (image[:,:,2] <= BLUE_THRESH_HI)
    
    return (redMask, greenMask, blueMask)
    
def apply_rgb_thresholds(image, rThresh = [(120, 180)], gThresh = [(120, 199)], bThresh = [(120, 190)]):
    """ Thresholds image using RGB ranges and returns a mask. This mimics apply_rgb_threshold, but
    thresholds a list of rgb Thresholds. The resultant binary mask is the OR of all the threshold masks."""
    #LVH - check if lists are equal
    nThresh = len(rThresh)
    if len(gThresh) != nThresh or len(bThresh) != nThresh:
        #apply exception functions
        pass
    
    #LVH - get binary masks for each rgb thresholds
    masks = []
    for it in xrange(nThresh):
        rmask, gmask, bmask = \
            apply_rgb_threshold(image, rThresh=rThresh[it], gThresh=gThresh[it], bThresh=bThresh[it])
        masks.append(rmask & gmask & bmask)
        
    #LVH - combine all binary masks
    combinedMask = masks[0]
    for tmpMask in masks[1:]:
        combinedMask = combinedMask | tmpMask
    
    return combinedMask

def get_threshold_mask(image):
    """ Summary function to gets the RGB thresholds for Kidney slide, 64x downsampled. """
    #LVH - get blobs of "signal" in the RGB channels
    rMask, gMask, bMask = apply_rgb_threshold(image)
    rgbMask = (rMask | gMask | bMask)
    
    #LVH - delete blobs that don't contribute
    shadowMask = get_shadow_mask(image)               #LVH - dark/light shadows, usually circular
    blueSpotMask = get_bluespot_mask(image)           #LVH - one image had a large blue blob...
    greenSpotMask = get_greenspot_mask(image)         #LVH - one image had a large green cactus-blob (like from FFX)
    purpleSpotMask = get_lightpurple_mask(image)      #LVH - some have light purple blobs, probably spilled dye
    
    #LVH - combine all thresholds
    imageMask = rgbMask & ~(shadowMask) & ~(blueSpotMask) & ~(greenSpotMask) & ~(purpleSpotMask)
    
    return imageMask
    
def get_shadow_mask(image):
    """ Thresholds image for light & dark shadows. Default values were optimized for PAS dye @ lvl 6 [64x ds].
    Returns a mask where 1 = where the shadows were located and 0 = where the shadows were not."""    
    #LVH -     light spots   dark spots
    rThreshs = [(175, 202), (152, 155)]
    gThreshs = [(174, 199), (153, 159)]
    bThreshs = [(170, 196), (159, 162)]
    
    return apply_rgb_thresholds(image, rThresh=rThreshs, gThresh=gThreshs, bThresh=bThreshs)

def get_bluespot_mask(image):
    """ Thresholds image for blue spot. Returns a mask of where 1 = where the blue sptots 
    were located and 0 = where the blue spots were not. Applies blue thresholds (light and dark) and
    does dilation + binary hole fill. These were ad-hoc thresholds & morphological operations,
    do not expect this to generalize well. Optimized for PAS dyes & lvl 6 [64x ds]"""
    #LVH -     light bspots dark bspots
    rThreshs = [(140, 185), (0  , 10 )]
    gThreshs = [(169, 199), (31 , 69 )]
    bThreshs = [(212, 222), (148, 192)]
    
    #LVH - get blue spots thresholds
    blueSpotThresh = apply_rgb_thresholds(image, rThresh=rThreshs, gThresh=gThreshs, bThresh=bThreshs)
     
    #LVH - apply morphology
    dilSelem = skm.rectangle(5,5)
    eroSelem = skm.rectangle(3,3)
    
    return dil_fil_ero(blueSpotThresh, dilSelem=dilSelem, fillStruct=None, eroSelem=eroSelem)   
  
def get_greenspot_mask(image):
    """ Thresholds image for green spot. Returns a mask of where 1 = where the blue sptots 
    were located and 0 = where the blue spots were not. Applies green thresholds (light, mid, dark) and
    does dilation + binary hole fill. These were ad-hoc thresholds & morphological operations,
    do not expect this to generalize well. Optimized for PAS dyes & lvl 6 [64x ds]"""
    #LVH -       dark g        mid g      light g   regions
    rThreshs = [(31 , 88 ), (110, 118), (130, 179)]
    gThreshs = [(106, 135), (165, 184), (187, 209)]
    bThreshs = [(140, 165), (145, 162), (159, 199)]
    
    #LVH - get blue spots thresholds
    greenSpotThresh = apply_rgb_thresholds(image, rThresh=rThreshs, gThresh=gThreshs, bThresh=bThreshs)
     
    #LVH - apply morphology
    dilSelem = skm.rectangle(4,5)
    eroSelem = skm.rectangle(3,3)
    
    return dil_fil_ero(greenSpotThresh, dilSelem=dilSelem, fillStruct=None, eroSelem=eroSelem)   

def get_lightpurple_mask(image):
    """ Thresholds image for light purple spots. Similar to green & blue spot detections """
    #LVH -     light purp   dark purp  light-grey purp
    rThreshs = [(205, 213), (196, 203), (201, 208)]
    gThreshs = [(181, 196), (167, 174), (192, 199)]
    bThreshs = [(193, 200), (185, 191), (197, 203)]
    
    #LVH - get light purple spot thresholds
    purpleSpotThresh = apply_rgb_thresholds(image, rThresh=rThreshs, gThresh=gThreshs, bThresh=bThreshs)
    
    #LVH - apply morphology
    dilSelem = skm.rectangle(3,4)
    eroSelem = skm.rectangle(3,3)
    
    return dil_fil_ero(purpleSpotThresh, dilSelem=dilSelem, fillStruct=None, eroSelem=eroSelem)   
    
#==============================================================================
# morphology functions
#==============================================================================
defaultDilSelem = skm.rectangle(5, 5)
defaultEroSelem = skm.rectangle(3, 3)
def dil_fil_ero(mask, dilSelem = defaultDilSelem, fillStruct = None, eroSelem = defaultEroSelem):
    """ Applys dilation, fills in holes, then erodes. Essentially a binary closing, but 
    attempts to fill in between and provides flexibility between dil & ero params.
    
    Parameters
    -----
    mask: ndarray
        Binary input mask.
        
    dilSelem: ndarray, optional
        For dilation. The neighborhood expressed as a 2-D array of 1’s and 0’s. 
        If default, uses generic skm.rectangle(5,5)
        If None, use cross-shaped structuring element (connectivity=1).
    
    fillStruct:structure : array_like, optional
        Structuring element used in the computation; large-size elements make 
        computations faster but may miss holes separated from the background 
        by thin regions. The default element (with a square connectivity equal to one) 
        yields the intuitive result where all holes in the input have been filled.
        
    eroSelem:ndarray, optional
        For erosion. The neighborhood expressed as a 2-D array of 1’s and 0’s. 
        If default, uses generic skm.rectangle(3,3)
        If None, use cross-shaped structuring element (connectivity=1).
    
    Returns
    -----
    dilfilero: ndarray
        Binary output mask.
    """
    dil = skm.binary_dilation(mask, selem=dilSelem)
    fill = ndi.binary_fill_holes(dil, structure=fillStruct)
    ero = skm.binary_erosion(fill, selem=eroSelem)
    return ero
    
def get_64ds_morphology_mask(mask):
    """ Applies morphological operations on post RGB-thresholded image masks. Optimized for
    slide level 6 => 64x downsample. Returns a list of binary masks, illustrating
    the procedure"""
    #LVH - fill in holes 1
    fill = ndi.binary_fill_holes(mask)
    
    #LVH - do erosion 1
    EROWIDTH = 3
    EROHEIGHT = 6 #5
    selem = skm.rectangle(EROWIDTH, EROHEIGHT)
    ero = skm.binary_erosion(fill, selem)
    
    #LVH - do dilation 1
    DILWIDTH = 4
    DILHEIGHT = 5
    selem = skm.rectangle(DILWIDTH, DILHEIGHT)
    dil = skm.binary_dilation(ero, selem)

    #LVH - fill in holes 2
    fill2 = ndi.binary_fill_holes(dil)
    
    #LVH - delete small blobs
    MIN_BLOB_SIZE = 150 
    rso = skm.remove_small_objects(fill2, min_size=MIN_BLOB_SIZE)
    
    #LVH - do binary closing
    BCWIDTH = 10
    BCHEIGHT = 10
    selem = skm.rectangle(BCWIDTH, BCHEIGHT)
    bc = skm.binary_closing(rso, selem)
    
    #LVH - do erosion 2
    EROWIDTH2 = 12 #4
    EROHEIGHT2 = 13 #5
    selem = skm.rectangle(EROWIDTH2, EROHEIGHT2)
    ero2 = skm.binary_erosion(bc, selem)
    
    #LVH - fill in holes 3
    fill3 = ndi.binary_fill_holes(ero2)

    #LVH - delete small blobs
    MIN_BLOB_SIZE2 = 600 #800 #825 #650
    rso2 = skm.remove_small_objects(fill3, min_size=MIN_BLOB_SIZE2) 
    
    ret = zip([  fill,    ero,   dil,    fill2,   rso,    bc,    ero2,   fill3,   rso2], 
              ['fill1', 'ero1', 'dil1', 'fill2', 'rso1', 'bc1', 'ero2', 'fill3', 'rso2'])
    return ret

#==============================================================================
# general image utililities
#==============================================================================
from PIL import Image
def rotateImage(image, angle, rotateKwargs = dict(resample = 0, expand = 0)):
    """ Rotates a numpy array image using PIL's rotation function """
    image = Image.fromarray(image)
    image = image.rotate(angle, **rotateKwargs)
    return np.array(image)
    
def resizeImage(image, (width, height), resample = 2): #LVH 0 - nearest neighor, 2 bilinear, 3 = bicubic spline.
    """ Resizes a numpy array image using PIL's rescale function """
    if image.dtype != bool:
        image = Image.fromarray(image)
        image = image.resize((width, height), resample = resample)
        return np.array(image)
    else:
        image = np.array(image, dtype = np.uint8)
        image[image > 0] = 255
        image = Image.fromarray(image)
        image = image.resize((width, height), resample = resample)
        return np.array(image, dtype = bool)
    