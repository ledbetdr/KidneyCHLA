"""
TODO: For large resolution images, compute histograms using parallel processing
"""

import bokeh.models as bkm
import bokeh.plotting as bkp
import numpy as np
from PIL import Image

def notBool(tf):
    if tf:
        tf = False
    else:
        tf = True
    return tf
    
def convertToRGBA(img):
    """ Converts img to RGBA """
    try: 
        if img.mode != "RGBA":
            img = img.convert("RGBA")
    except AttributeError:
        try:
            img = Image.fromarray(img)
            if img.mode != "RGBA":
                img = img.convert("RGBA")
        except:
            print 'Error: Image is not of proper numpy arr (typically dtype = np.uint8. Error in converting arr to PIL.Image.'
            return None
    return np.array(img)
     
def resizeImg(img, newWidth):
    """ Resizes a PIL.Image to maintain aspect ratio with with width = newWidth """
    wPercent = (newWidth/float(img.size[0]))
    hSize = int((float(img.size[1])*float(wPercent)))
    return img.resize( ( newWidth, hSize ), Image.ANTIALIAS )  
    
def bkp_preprocImg(img, flipUP = True):
    """ Preprocess a image arr (numpy or PIL.Image) to be absorbed bokeh's plotting."""
    #LVH - convert to RGBA if it's not, depending on whether numpy.arr or PIL.Image
    img = convertToRGBA(img)

    #LVH - traditional image conventions refer to top left corner as 0,0. 
    #    - however, bokeh has bottom left (0,0).
    flippedImg = np.flipud(img)
    
    #LVH - convert img to be 2d arr (M x N) with dtype = uint32 -- for bokeh
    bimg = np.squeeze(flippedImg.astype(np.uint8).view(np.uint32)) 
    return (flippedImg, bimg)

def bkp_imgHist(img, channels, bins, figKwargs = {}, pltHistKwargs = {}):
    """ Gets 3 channel histograms for an img (RGBA, CMKY, YCbCr) 
    
    Parameters
    ------
    img: numpy arr
        3 or 4 Channel Image. Doesn't take grey-scale.
    
    channels: list
        List of bokeh plotting keywords passed to bokeh's plotting to each histogram for unique-referencing
        e.g.: 
            RGBchannels = [
                           dict(color = '#FF4848', line_color = 'red'  , name = 'rhist', legend = 'red'),
                           dict(color = '#27DE55', line_color = 'green', name = 'ghist', legend = 'green'),
                           dict(color = '#2966B8', line_color = 'blue' , name = 'bhist', legend = 'blue'),
                          ]
    
    bins: int, list
        Number of bins when forming histogram
    
    figKwargs: dict
        Bokeh keywords passed to bokeh.plotting.figure
    
    pltHistKwargs: dict
        Bokey keywords passed to bokeh's plotting. This is appended to the list of "channels"
    
    """
    # set up bokeh figure for histogram
    ph = bkp.figure(**figKwargs)
    kwargs = pltHistKwargs
    
    # chan1 (red)
    rImg = img[:,:,0].flatten()
    rhist, redges = np.histogram( rImg, bins = bins)
    rmax = max(rhist) * 1.1
    for key, item in channels[0].iteritems(): kwargs[key] = item
    ph.quad(bottom=0, left=redges[:-1], right=redges[1:], top=rhist, **kwargs)
            
    # char2 (green)
    gImg = img[:,:,1].flatten()
    ghist, gedges = np.histogram( gImg, bins = bins)
    gmax = max(ghist) * 1.1
    for key, item in channels[1].iteritems(): kwargs[key] = item
    ph.quad(bottom=0, left=gedges[:-1], right=redges[1:], top=ghist, **kwargs)
    
    # chan3 (blue)
    bImg = img[:,:,2].flatten()
    bhist, bedges = np.histogram( bImg, bins = bins)
    bmax = max(bhist) * 1.1
    for key, item in channels[2].iteritems(): kwargs[key] = item
    ph.quad(bottom=0, left=bedges[:-1], right=bedges[1:], top=bhist, **kwargs)
    
    # set plot range to view histogram nicely
    ph.y_range.start = 0
    ph.y_range.end =  np.max( [bmax, gmax, rmax] ) * 1.1
    
    # get histogram data sources
    r_source = ph.select(dict(name=channels[0]['name']))[0].data_source
    g_source = ph.select(dict(name=channels[1]['name']))[0].data_source
    b_source = ph.select(dict(name=channels[2]['name']))[0].data_source
    return (ph, r_source, g_source, b_source)
  
def getInteractiveHist(img, uid, MAX_WIDTH = None, autoOpen = True):
    """ Given img (PIL.Image), sends requests/forwards data to a bokeh-server for interactive histogram 
    
    Parameters
    ------
    img: PIL.Image
        img to be plotted
    
    MAX_WIDTH: int
        For computational efficiency, downsample image to have width -> MAX_WIDTH, while maintiang aspect ratio.
    
    autoOpen: bool
        If True, will open HTML to specified server. Else, navigate there yourself: localhost:5006 by default
    """
    #LVH - preprocess image for bokeh
    if img.size[0] > MAX_WIDTH and MAX_WIDTH is not None:
        print '... Image larger than %i, resizing width to this [while maintaining aspect ratio]'%(MAX_WIDTH)
        img = resizeImg(img, newWidth=MAX_WIDTH)
    flippedImg, bimg = bkp_preprocImg(img, flipUP=True) 
    rgb = np.array(img)
    
    # LVH - set up height/width / scalings. This translation proves to be troublesome later...
    iheight, iwidth = rgb.shape[:2] # for npy/matplotlib, x=height=rowsm y=width=columns
    width, height = bimg.shape[:2]  # for bokeh: x=width=rows, y=height=columns 
    bwidth, bheight = ( width/2 , height/2 ) #LVH - bokeh plots img on arr.shape/2 for some reason
    
    #==============================================================================
    # #LVH - set up bokeh server and plot
    #==============================================================================
    bkp.output_server("Interactive_Image_Histogram")
    #bkp.output_file("Interactive_Image_Histogram.html")
    p = bkp.figure(title = uid, plot_width=1000, plot_height= int(iheight * (650/float(iwidth))), x_range = [0, bwidth], y_range = [0, bheight],
                   min_border_left = 10, min_border_right = 30, min_border_top = 10, min_border_bottom = 10)
    box_select_tool = bkm.tools.BoxSelectTool(select_every_mousemove = False)
    p.add_tools(box_select_tool)
    
    #LVH - plot image
    p.image_rgba( image = [bimg], x = [0], y = [0], dw = [bwidth], dh = [bheight] )
    p.xaxis[0].ticker=bkm.FixedTicker(ticks=[])
    p.yaxis[0].ticker=bkm.FixedTicker(ticks=[])
    
    #LVH - set up  & plot box-selected-drawn-rectangle
    source = bkp.ColumnDataSource(data=dict(x = [], y = [], width = [], height = []))
    p.rect(x = 'x', y = 'y' , width = 'width', height = 'height', source = source, 
           fill_alpha = 0.2, line_alpha = 0.7, line_color = "#3A5785", name = 'ROI')
    
    #LVH - create the histogram for each "color". This works for arbitrary color schemes.
    #    - however, note that  we must plot the image in RGBA. Will check if other colors work later.
    bins = np.arange(0, 256)
    figKwargs = dict(plot_width = 1000, plot_height = 300, x_range = [0,255], toolbar_location="right",
                     min_border_left = 5, min_border_right = 10, min_border_top = 0, min_border_bottom = 0)
    histKwargs = dict(fill_alpha = 0.5, line_width = 2, line_alpha = 0.0)
    
    # RGB data & source
    RGBchannels = [
                dict(color = '#FF4848', name = 'rhist', legend = 'red'),
                dict(color = '#27DE55', name = 'ghist', legend = 'green'),
                dict(color = '#2966B8', name = 'bhist', legend = 'blue'),
                ]
    
    rgbFig, rgbRsource, rgbGsource, rgbBsource = \
                    bkp_imgHist(rgb, RGBchannels, bins, figKwargs=figKwargs, pltHistKwargs=histKwargs)
    rgbFig.yaxis[0].ticker=bkm.FixedTicker(ticks=[])
    rgbFig.legend.orientation = "top_left"
    
    #LVH - set up 
    def change_red_alpha(obj, attrname, old, new):
        renderer = rgbFig.select(dict(name=RGBchannels[0]['name']))[0]
        renderer.glyph.fill_alpha = new
        bkp.cursession().store_objects(renderer)
        
    redSlider = bkm.widgets.Slider(title="Red", name = "Red", value = histKwargs['fill_alpha'],
                                 start = 0, end = 1.0, step = 0.1)
                                 
    redSlider.on_change('value', change_red_alpha)
    
    def change_green_alpha(obj, attrname, old, new):
        renderer = rgbFig.select(dict(name=RGBchannels[1]['name']))[0]
        renderer.glyph.fill_alpha = new
        bkp.cursession().store_objects(renderer)
        
    greenSlider = bkm.widgets.Slider(title="Green", name = "Green", value = histKwargs['fill_alpha'],
                                 start = 0, end = 1.0, step = 0.1)
                                 
    greenSlider.on_change('value', change_green_alpha)
    
    def change_blue_alpha(obj, attrname, old, new):
        renderer = rgbFig.select(dict(name=RGBchannels[2]['name']))[0]
        renderer.glyph.fill_alpha = new
        bkp.cursession().store_objects(renderer)
        
    blueSlider = bkm.widgets.Slider(title="Blue", name = "Blue", value = histKwargs['fill_alpha'],
                                 start = 0, end = 1.0, step = 0.1)
                                 
    blueSlider.on_change('value', change_blue_alpha)
    
    #==============================================================================
    # #LVH - set up server requests/forwarding
    #==============================================================================
    def on_selection_change(obj, attr, old, new):
        geometry = new[0] # Hopefully 0 stays as the box-select toolEvent...
        if geometry['type'] == 'rect':
            x0 = geometry['x0'] # bottom left bokeh
            y0 = geometry['y0'] # bottom left bokeh
            x1 = geometry['x1']
            y1 = geometry['y1']
    
            #LVH - draw rectangle based on box-selection tool (Note: in bokeh's coordinates)
            width = x1 - x0
            height = y1 - y0
            x = x0 + width/2  # refers to the middle of the rectangle
            y = y0 + height/2 # refers to the middle of the rectangle
            source.data['x'] = [x]
            source.data['y'] = [y]
            source.data['width'] = [width]
            source.data['height'] = [height]
            
            #LVH - get ROI of image and compute histograms. First... convert x,y to img coordinates
            #LVH - bokeh reverses height/width dimensions when plotting AND 
            #    - scales by 0.5 of actual dimensions of the image.
            xi0 = int(x0/float(bwidth) * iwidth)
            xi1 = int(x1/float(bwidth) * iwidth)
            #LVH - account for the flipping difference between bokeh & img y coordinates
            yi0 = iheight - int(y1/float(bheight) * iheight)
            yi1 = iheight - int(y0/float(bheight) * iheight)
            
            rhist, _ = np.histogram(rgb[yi0:yi1, xi0:xi1, 0].flatten(), bins = bins)
            ghist, _ = np.histogram(rgb[yi0:yi1, xi0:xi1, 1].flatten(), bins = bins)
            bhist, _ = np.histogram(rgb[yi0:yi1, xi0:xi1, 2].flatten(), bins = bins)
                
            rgbRsource.data["top"] = rhist
            rgbGsource.data["top"] = ghist
            rgbBsource.data["top"] = bhist
            rhmax = np.max( [max(rhist[1:]), max(ghist[1:]), max(bhist[1:])] )
            rgbFig.y_range.end = rhmax * 1.1
        
            #LVH - forward server updated variables -- this is the most time-consuming part. + latency
            bkp.cursession().store_objects(source, rgbFig)
        
    #LVH - activate server requests given change in tool events (box-select-tool)
    toolEvents = p.tool_events
    toolEvents.on_change('geometries', on_selection_change)
    
    ##LVH - set up layout of our figures
    histLayout = bkp.hplot(rgbFig, bkp.vplot(redSlider, greenSlider, blueSlider))
    layout = bkp.vplot(p, histLayout) 
    if autoOpen:
        bkp.show(layout) 
    
    
    #LVH - keeps the script up to take requests from server to update variables
    bkp.cursession().poll_document(bkp.curdoc(), 0.01)

if __name__ == '__main__':
    #LVH - load up test image for fun
#    imgPath = "../../Test/cat.jpg"
#    img = Image.open(imgPath) 
    
#    LVH - load up kidney slide
    import openslide
    import glob
    import os
    slideDir = '../../Data/'#'/media/damiauzo/Kidney/'
    slidePath = np.random.choice(glob.glob(os.path.join(slideDir, '*.ndpi')), size = 1)[0]

    slide = openslide.OpenSlide(slidePath)
    uid = os.path.basename(slidePath).split('.')[0]
    SLIDELVL = 3 #4
    img = slide.read_region((0, 0), slide.level_count - SLIDELVL, slide.level_dimensions[slide.level_count - SLIDELVL])  
    img = np.array(img)
    
    import segmentUtils as su
    imageMask = su.get_threshold_mask(img)
    img[~imageMask] = 255
    
    #LVH - run interactive histogram. Requires PIL.Image
    getInteractiveHist(img, uid, MAX_WIDTH=None, autoOpen=False) # set MAX_WIDTH = None if you think you can handle it