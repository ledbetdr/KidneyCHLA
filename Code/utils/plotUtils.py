# -*- coding: utf-8 -*-
"""
Created on Mon Sep 28 23:52:34 2015

@author: damiauzo
"""
import numpy as np

# initialize matplotlib backend & figure for using `screen -r`
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
plt.ioff()
plt.figure(1, figsize=(10, 8))
plt.clf()


def _check_convert_RGB(image):
    """ Converts image (PIL.Image) to RGB. If img is numpy array,
    assume already RGB. Returns numpy arr of color type RGB"""
    try:  # LVH - more general for numpy array or PIL.Image
        if image.mode != 'RGB':
            image = image.convert('RGB')
    except:
        pass  # LVH - assume image is a numpy array of RGB already

    image = np.array(image)
    if image.shape[2] != 3:
        raise Exception("Cannot properly convert image to RGB")

    return image

def plotRGB(image, axes = None, imshowKwargs = {}):
    """ Plots the RGB channels of the image.

    Parameters
    ------
    image: ndarray or PIL.Image
        Image whose RGB channels will be plotted.

    axes: plt.Axes, default = None
        If provided, will plot along these axes provided.
        len(axes) must equal 3, or will raise error

    imshowKwargs: dict
        Keyward arguments sent to plt.imshow
    """
    #LVH - check if len(axes) == 3; else creates a figure
    try:
        if len(axes) != 3:
            print("axes provided does not have len == 3 to plot RGB. "
                  "Creating new figure")
    except:
        fig, axes = plt.subplots(nrows=3, ncols=1, figsize=(20, 12))

    # LVH - if no cmap provided, will use grayscale
    if 'cmap' not in imshowKwargs.keys():
        imshowKwargs['cmap'] = 'gray'

    # LVH - check if PIL or npy, convert to npy to slice
    image = _check_convert_RGB(image)

    # LVH - plot RGB channels
    ax = axes[0]
    ax.imshow(image[:, :, 0], **imshowKwargs)
    ax.set_title("Red")
    ax.yaxis.set_visible(False)
    ax.xaxis.set_visible(False)
    ax = axes[1]
    ax.imshow(image[:, :, 1], **imshowKwargs)
    ax.set_title("Green")
    ax.yaxis.set_visible(False)
    ax.xaxis.set_visible(False)
    ax = axes[2]
    ax.imshow(image[:, :, 2], **imshowKwargs)
    ax.set_title("Blue")
    ax.yaxis.set_visible(False)
    ax.xaxis.set_visible(False)


def plotColorSchemes(image):
    """Plot the 3 channels of `imgage` in different subplots"""
    # LVH - convert slidePlot to different color formats and numpy arr
    rgb = np.array(image.convert('RGB'))
    cmyk = np.array(image.convert('CMYK'))
    ycbcr = np.array(image.convert('YCbCr'))

    # LVH - create subplots beforehand
    fig, axes = plt.subplots(nrows=3, ncols=3, num=1)

    # DRL - RGB
    ax = axes[0, 0]
    ax.imshow(rgb[:, :, 0])
    ax.xaxis.set_visible(False)
    ax.yaxis.set_visible(False)
    ax.set_title('Red')

    ax = axes[1, 0]
    ax.imshow(rgb[:, :, 1])
    ax.xaxis.set_visible(False)
    ax.yaxis.set_visible(False)
    ax.set_title('Green')

    ax = axes[2, 0]
    ax.imshow(rgb[:, :, 2])
    ax.xaxis.set_visible(False)
    ax.yaxis.set_visible(False)
    ax.set_title('Blue')

    # DRL - CMY
    ax = axes[0, 1]
    ax.imshow(cmyk[:, :, 0])
    ax.xaxis.set_visible(False)
    ax.yaxis.set_visible(False)
    ax.set_title('Cyan')

    ax = axes[1, 1]
    ax.imshow(cmyk[:, :, 1])
    ax.xaxis.set_visible(False)
    ax.yaxis.set_visible(False)
    ax.set_title('Magenta')

    ax = axes[2, 1]
    ax.imshow(cmyk[:, :, 2])
    ax.xaxis.set_visible(False)
    ax.yaxis.set_visible(False)
    ax.set_title('Yellow')

    # DRL - YCbCr
    ax = axes[0, 2]
    ax.imshow(ycbcr[:, :, 0])
    ax.xaxis.set_visible(False)
    ax.yaxis.set_visible(False)
    ax.set_title('Chroma')

    ax = axes[1, 2]
    ax.imshow(ycbcr[:, :, 1])
    ax.xaxis.set_visible(False)
    ax.yaxis.set_visible(False)
    ax.set_title('Cb')

    ax = axes[2, 2]
    ax.imshow(ycbcr[:, :, 2])
    ax.xaxis.set_visible(False)
    ax.yaxis.set_visible(False)
    ax.set_title('Cr')

    plt.tight_layout()


def plotSchemeHists(image, bins=64, alpha=.4):
    """Plot histogram for 3 channels of image"""
    # LVH - convert slidePlot to different color formats and numpy arr
    rgb = np.array(image.convert('RGB'))
    cmyk = np.array(image.convert('CMYK'))
    ycbcr = np.array(image.convert('YCbCr'))

    # LVH - create subplots beforehand
    fig, axes = plt.subplots(nrows=2, ncols=2, num=1)

    # DRL - overall plot
    ax = axes[0, 0]
    ax.imshow(image)
    ax.xaxis.set_visible(False)
    ax.yaxis.set_visible(False)
    ax.set_title('Raw Slide')

    # DRL - histogram for rgb
    ax = axes[0, 1]
    ax.hist(rgb[:, :, 0].flatten(), bins=bins, range=[0,255], color='red', alpha=alpha, log=True, label='Red')
    ax.hist(rgb[:, :, 1].flatten(), bins=bins, range=[0,255], color='green', alpha=alpha, log=True, label='Green')
    ax.hist(rgb[:, :, 2].flatten(), bins=bins, range=[0,255], color='blue', alpha=alpha, log=True, label='Blue')
    ax.legend()
    ax.set_title('RGB Histogram')

    # DRL - histogram for CMYK
    ax = axes[1, 0]
    ax.hist(cmyk[:, :, 0].flatten(), bins=bins, range=[0,255], color='cyan', alpha=alpha, log=True, label='Cyan')
    ax.hist(cmyk[:, :, 1].flatten(), bins=bins, range=[0,255], color='magenta', alpha=alpha, log=True, label='Magenta')
    ax.hist(cmyk[:, :, 2].flatten(), bins=bins, range=[0,255], color='yellow', alpha=alpha, log=True, label='Yellow')
    ax.legend()
    ax.set_title('CMYK Histogram')

    #DRL - histogram for ycbcr
    ax = axes[1, 1]
    ax.hist(ycbcr[:, :, 0].flatten(), bins=bins, range=[0,255], color='yellow', alpha=alpha, log=True, label='Luma')
    ax.hist(ycbcr[:, :, 1].flatten(), bins=bins, range=[0,255], color='blue', alpha=alpha, log=True, label='Change Blue')
    ax.hist(ycbcr[:, :, 2].flatten(), bins=bins, range=[0,255], color='red', alpha=alpha, log=True, label='Change Red')
    ax.legend()
    ax.set_title('yCbCr Histogram')

    plt.tight_layout()


def plot_regression(df, groupby_col='patientstudyID', pred_col='12_month_pred',
                    truth_col='12_month', ax=None, save_path=None):
    """Plots the average prediction over an axis, `groupby_col`. This is because
    a there may be multiple predictions per unique item in groupby_col, but
    only one corresponding truth. This takes the average of the prediction
    and plots truth vs pred regression plot to visualize of model performance.
    Default values are set for KidneyCHLA.

    Parameters
    ------
    df: pd.DataFrame
        Dataframe containing truth, prediction, and column to take groupby avg

    groupby_col: str, default=patientstudyID
        Column to take a grouby avg.

    pred_col: str, default=12_month_pred
        Column containing prediction

    truth_col: str, default=12_month
        Column containing truth

    ax: matplotlib axes, default=None
        Axes to plot on. If None, will create a new plot on figure(100).

    save_path: str, default=None,
        Path to save plot. If None, will not save.

    Returns
    ------
    ax: matplotlib axes
        Axes that regression was plotted on.
    """
    plt.xkcd()


    # set up data
    df = df.groupby(groupby_col).agg([np.mean, np.std])
    avg_pred = df[pred_col]['mean'].values
    std_pred = df[pred_col]['std'].values
    truth = df[truth_col]['mean'].values

    # check errors
    if ax is None:
        plt.figure(100, figsize=(10, 8))
        plt.clf()
        fig, ax = plt.subplots(num=100)

    if len(avg_pred) != len(truth):
        raise Exception("Length of predictions must equal length of truth")

    # reorder from lowest to greatest
    idx_sort = np.argsort(truth)
    truth = truth[idx_sort]
    avg_pred = avg_pred[idx_sort]
    std_pred = std_pred[idx_sort]

    # plot
    ax.set_title("%s: Truth vs Model Regression" % truth_col, fontsize=20)
    ax.scatter(truth, avg_pred, marker='o', c='m', alpha=0.7, s=50, label='truth_vs_pred')
    ax.scatter(truth, avg_pred - std_pred, marker='_', c='m', alpha=0.70, s=175)
    ax.scatter(truth, avg_pred + std_pred, marker='_', c='m', alpha=0.70, s=175)

    # plot error bars
    for x in range(len(truth)):
        ax.vlines(truth[x], avg_pred[x] - std_pred[x],
                  avg_pred[x] + std_pred[x], linestyle='--',
                  alpha=0.70, colors='orange', linewidth=1)

    # plot idea line
    max_val = np.max([max(avg_pred), max(truth)])
    ax.plot([0, max_val], [0, max_val], linestyle='-',
            linewidth=2, c='o', alpha=0.6, label='perfect')

    # label axes
    ax.set_xlabel('Truth Prediction of eGFR at %s' % truth_col, fontsize=16)
    ax.set_ylabel('Model Prediction of eGFR at %s' % truth_col, fontsize=16)
    plt.legend(loc='upper left', fontsize=16)
    plt.tight_layout()

    if save_path is not None:
        plt.savefig(save_path, bbox_inches='tight')

    return ax
