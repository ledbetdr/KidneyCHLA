"""
Plots network error specifically to KidneyCHLA workflow. Can be generalized.

args
--------
errors_path: str
    Path to errors.csv output from Network_Monitor class.

output_path: str, optional, default='../Shared/curNet.png'
    Path to save figure

plot_xkcd: bool, optional, deault=True
    Wehther to plot in XKCD or not.

"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import time
plt.ioff()


def plot_run_errors(error_path, ax=None, y_label='Mean Squared Error',
                    train_kwargs={'color': 'm'},
                    valid_kwargs={'color': 'salmon'},
                    lr_kwargs={'colors': 'orange'}):
    """
    Plots train, validation, and learning rate from errors_path format.
    Particular to Monitor_Network's saving. More information can be provided
    to kwargs for more general errors_path dataframes. By default,
    train, valid, lr are implied from the dataframes columns [1,2,3]
    respectively. Epochs are implied from index. To specify more specific
    columns for train,valid, lr, within *_kwargs, specify *_col key to the
    corresponding column in errors_path.
    """
    # Load errors data
    errorDF = pd.read_csv(error_path)

    # Get plot axes if not provided
    if ax is None:
        fig, ax = plt.subplots(figsize=(12, 8))

    # Infer epoch from index.
    epoch = np.arange(len(errorDF))  # errorDF['epoch'].values

    # Get columns fro kwargs, else infer from errors_path column indexes 1,2
    train_col = train_kwargs.pop('train_col', errorDF.columns[1])
    valid_col = valid_kwargs.pop('valid_col', errorDF.columns[2])

    # Get loss values to plot
    train_loss = errorDF[train_col].values
    valid_loss = errorDF[valid_col].values

    # Plot constants
    X_PAD = 1.5
    Y_PAD = 0.3
    fs = 20

    # Plot losses
    all_max = np.max([max(valid_loss), max(train_loss)])
    ax.scatter(epoch, train_loss, alpha=0.5, s=75, marker='o', label='train', **train_kwargs)
    ax.plot(epoch, train_loss, alpha=0.2, linestyle='--', linewidth=1, **train_kwargs)
    ax.scatter(epoch, valid_loss, alpha=0.5, s=75, marker='o', label='valid', **valid_kwargs)
    ax.plot(epoch, valid_loss, alpha=0.2, linestyle='--', linewidth=1, **valid_kwargs)

    # Plot learning rate, if provided.
    if lr_kwargs is not None:
        # Infer learning_rate column if not in kwargs
        lr_col = lr_kwargs.pop('lr_col', errorDF.columns[3])

        # Only get unique learning_rates on the first appearance
        tmpDF = errorDF.drop_duplicates(subset=[lr_col])

        # Plot learning rates & value texts
        ax.vlines(tmpDF.index, -Y_PAD, all_max + Y_PAD,
                  label='lr', linestyle='-.', **lr_kwargs)
        for x, lr in zip(tmpDF.index, tmpDF[lr_col]):
            ax.text(x + 0.25, all_max * (float(6))/7,
                    '%.2E' % lr, fontsize=(fs - 10))

    # Edit plot canvas
    ax.set_xlim(-X_PAD, epoch[-1] + X_PAD)
    ax.set_ylim(-Y_PAD,  all_max + Y_PAD)
    ax.set_xlabel('Epoch', fontsize=fs)
    ax.set_ylabel(y_label, fontsize=fs)
    ax.set_title('Network Error', fontsize=(fs+8))
    plt.legend(loc='lower left')
    plt.tight_layout()

    return ax

if __name__ == '__main__':
    import argparse
    from utils import argUtils

    # parse command line arguments
    p = argparse.ArgumentParser()
    p.add_argument('--errors_path',         type=str)
    p.add_argument('--output_path',         type=str,                   default='../Shared/curNet.png')
    p.add_argument('--plot_xkcd',           type=argUtils.bool_parser,  default=True)
    args = vars(p.parse_args()) # parse into a dictionary.

    errors_path = args['errors_path']

    if args['plot_xkcd']:
        plt.xkcd()

    # Start plotting and updating every hour for monitoring
    WAIT_TIME = 1800
    while True:
        try:
            print('[%s] Uploading latest network error for %s'
                  % (time.strftime("%I:%M:%S"), errors_path))

            plt.figure(1, figsize=(12, 8))
            plt.clf()
            fig, ax = plt.subplots(num=1)
            ax = plot_run_errors(errors_path, ax=ax)
            plt.savefig(args['output_path'], bbox_inches='tight')
            time.sleep(WAIT_TIME)
        except KeyboardInterrupt:
            break
