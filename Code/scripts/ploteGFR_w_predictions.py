"""
Script to plot the eGFR measurements and the slope
"""
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import os
import sys

sys.path.insert(0, '../')
from utils import dataUtils as du

truthDF = pd.read_csv('../../Data/Truth/preProcessed_eGFR_truth.csv')
outputDir = '../../Shared'
if not os.path.exists(outputDir):
    os.mkdir(outputDir)

predDF = pd.read_csv('../../Shared/valid_predictions.csv')
predDF['patientstudyID'] = predDF['datapath'].apply(
                              lambda x: du.get_patientID_from_datapath(x))
predDF = predDF.groupby('patientstudyID').mean()

points = ['bias_eGFR',
          '4_month',
          '8_month',
          '12_month',
          '18_month',
          '24_month',
          '30_month',
          '36_month',
          '42_month',
          '48_month']

truthDF.set_index('patientstudyID', inplace=True)
truthDF['slope_eGFR_pred'] = np.NaN
truthDF.loc[predDF.index, 'slope_eGFR_pred'] = predDF['slope_eGFR_pred'].values
truthDF = truthDF.loc[predDF.index]
for i in xrange(len(truthDF)):
    patient = truthDF.iloc[i]
    x = np.array([0, 4, 8, 12, 18, 24, 30, 36, 42, 48])
    y = np.array(patient[points].values, dtype=float)
    valid_mask = ~np.isnan(y)
    ret = np.polyfit(x[valid_mask], y[valid_mask], 1)

    x_int = np.arange(0, 49)
    y_int = ret[0]*x_int + ret[1]

    fignum = 1
    fs = 16
    X_PAD = 3
    Y_PAD = 3
    plt.figure(fignum)
    plt.clf()
    fig, ax = plt.subplots(1, 1, num=fignum, sharex=True, squeeze=True)

    ax.scatter(x, y, color='m', s=75, marker='o', label='eGFR Measurements')
    ax.plot(x_int, y_int, color='b', linestyle='--',
            linewidth=2, label='Linear Regression')
    ax.set_xlim(-X_PAD, x[-1] + X_PAD)
    ax.set_ylim(np.min(y[valid_mask]) - Y_PAD, np.max(y[valid_mask]) + Y_PAD)
    ax.set_xlabel('Months since baseline', fontsize=fs)

    pred_y = x_int*patient['slope_eGFR_pred'] + ret[1]

    ax.plot(x_int, pred_y, color='g', linestyle='--',
            linewidth=2, label='CNN Prediction')
    plt.legend(loc='best')
    plt.tight_layout()
    plt.savefig(os.path.join(outputDir, '%s_eGFR_pred' % patient.name + '.png'),
                bbox_inches='tight', transparent=True)
