"""
Not all the image chips generated from v1 were of correct size. For example,
some where (700, 3) or something. Let's just delete these and figure out
why we get them wrong later.
"""
import glob
import os
import numpy as np

# path to KidneyDBv1 data (only fixing npy dir)
dataDir = '../../Data/KidneyDBv1/npy'


datapaths = glob.glob(os.path.join(dataDir, '*.npy'))
ndatapaths = len(datapaths)
# list to store incorrect datapaths
inc_datapaths = []
for idx, datapath in enumerate(datapaths):
    img = np.load(datapath)

    # if shape is not what we're expecting
    if img.shape != (700, 700, 3):
        inc_datapaths.append(datapath)

    if idx % 1000 == 0 and idx > 0:
        print '... on %i ouf ot %i' % (idx+1, ndatapaths)

# delete all datapaths with incorrect shape
for datapath in inc_datapaths:
    os.remove(datapath)
