"""
#LVH - driver script to segment Kidney cells

Segmentation is tuned for slide level 6 --> 64x downsample

TODO: look into skimage -> convex hull
"""

import glob
import os
import matplotlib.pyplot as plt
import openslide
import numpy as np
from PIL import Image


# LVH - local imports
import sys
sys.path.insert(0, "../")
from utils import segmentUtils as su
from utils import plotUtils as pu

# LVH - data paths
dataDir = r'/media/damiauzo/Projects/VPICU/Data/Kidney/TRI_PAS_data'
slidePaths = glob.glob(os.path.join(dataDir, '*PAS.ndpi'))
rng = np.random.RandomState(seed=17)
rng.shuffle(slidePaths)

# LVH - output path
runName = 'segmentationv12'
outDir = r'../../Output'
outPath = os.path.join(outDir, runName)
if not os.path.exists(outPath):
    os.mkdir(outPath)
outPath2 = os.path.join(outPath, 'stepbystep')
if not os.path.exists(outPath2):
    os.mkdir(outPath2)

# LVH - split train/test sets
SPLIT_TRAIN_TEST = False
if SPLIT_TRAIN_TEST:
    nSlides = len(slidePaths)
    nTrain = int(0.80*nSlides)
    trainSlides = slidePaths[:nTrain]
    testSlides = slidePaths[nTrain:]
else:
    trainSlides = slidePaths

# LVH - iterate through slides in training set
# trainSlides = [os.path.join(dataDir, '10_26609_025_501 L2 PAS.ndpi')]
for it, slidePath in enumerate(trainSlides):
    # LVH - read in slide
    slide = openslide.OpenSlide(slidePath)
    uid = os.path.basename(slidePath).split('.')[0]
    print('... working on %s. Iteration %i out of %i'
          % (uid, it, len(trainSlides)))

    print('... ... getting image slide')
    SLIDELVL = 6
    image = slide.read_region((0, 0), SLIDELVL,
                              slide.level_dimensions[SLIDELVL])

    width, height = image.size
    image = np.array(image.convert("RGB"))

    print('... ... applying thresholds')
    rMask, gMask, bMask = su.apply_rgb_threshold(image)
    # LVH - take the OR so that each channel contributes a "signal" blob
    rgbMask = (rMask | gMask | bMask)

    shadowMask = su.get_shadow_mask(image)
    blueSpotMask = su.get_bluespot_mask(image)
    greenSpotMask = su.get_greenspot_mask(image)
    purpleSpotMask = su.get_lightpurple_mask(image)

    imageMask = rgbMask & ~(shadowMask) & ~(blueSpotMask) & \
                   ~(greenSpotMask) & ~(purpleSpotMask)

    print('... ... applying morphological operations')
    morphMasks = su.get_64ds_morphology_mask(imageMask)
    nMorphs = len(morphMasks)

    print('... ... plotting segmentation procedure')
    # LVH - plot RGB Thresholds
    plt.figure(1, figsize=(20, 12))
    plt.clf()
    fig, axes = plt.subplots(nrows=4, ncols=2, num=1)
    pu.plotRGB(image, axes=axes[:3, 0])
    ax = axes[3, 0]
    ax.imshow(image)
    ax.set_title("RGB: img.size = (%i, %i)" % (width, height))
    ax.yaxis.set_visible(False)
    ax.xaxis.set_visible(False)

    pu.plotRGB(np.array([rMask, gMask, bMask]).transpose(1, 2, 0),
               axes=axes[:3, 1])
    ax = axes[3, 1]
    ax.imshow(imageMask, cmap='gray')
    ax.set_title("RGB Mask")
    ax.yaxis.set_visible(False)
    ax.xaxis.set_visible(False)
    fig.suptitle(uid, fontsize=30)
    plt.tight_layout()
    plt.savefig(os.path.join(outPath2, uid + '.png'),
                bbox_inches='tight', transparent=True)

    # LVH - Plot morphological operations part 1
    plt.figure(2, figsize=(20, 12))
    plt.clf()
    fig, axes = plt.subplots(nrows=4, ncols=1, num=2)
    for it, morph in enumerate([(imageMask, 'RGB Threshold Mask')] +
                               morphMasks[:3]):
        ax = axes[it]
        ax.imshow(morph[0], cmap='gray')
        ax.set_title(morph[1])
        ax.yaxis.set_visible(False)
        ax.xaxis.set_visible(False)
    plt.tight_layout()
    plt.savefig(os.path.join(outPath2, uid + '_morph_1.png'),
                bbox_inches='tight', transparent=True)

    # LVH - Plot morphological operations part 2
    plt.figure(3, figsize=(20, 12))
    plt.clf()
    fig, axes = plt.subplots(nrows=3, ncols=2, num=3)
    axes = axes.flatten()
    for it, morph in enumerate(morphMasks[3:]):
        ax = axes[it]
        ax.imshow(morph[0], cmap='gray')
        ax.set_title(morph[1])
        ax.yaxis.set_visible(False)
        ax.xaxis.set_visible(False)
    fig.suptitle(uid, fontsize=30)
    plt.tight_layout()
    plt.savefig(os.path.join(outPath2, uid + '_morph_2.png'),
                bbox_inches='tight', transparent=True)

    # LVH - plot the images with binary thresholds side by side in PIL format
    imageAfterThresh = image.copy()
    imageAfterThresh[~morphMasks[-1][0]] = 0
    if width < 2*height:
        cImage = np.zeros((height, width * 2, image.shape[2]), dtype=np.uint8)
        cImage[:, :width, :] = image
        cImage[:, width:, :] = imageAfterThresh
    else:
        cImage = np.zeros((height * 2, width, image.shape[2]), dtype=np.uint8)
        cImage[:height, :, :] = image
        cImage[height:, :, :] = imageAfterThresh

    cImage = Image.fromarray(cImage)
    cImage.save(os.path.join(outPath, uid + '.png'))

print('Successfully done')
