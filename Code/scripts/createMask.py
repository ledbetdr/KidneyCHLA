# -*- coding: utf-8 -*-
"""
Created on Sun Sep 27 23:50:20 2015

@author: damiauzo

DRL - create a mask for just the kidney regions of the image
"""
import os
import glob
import openslide
import numpy as np
import matplotlib.pyplot as plt
import utils.plotUtils as pUtils

#DRL - turn off interactive plotting so figures don't steal focus
plt.ioff()

#DRL - path to the main directory containing the ndpi files
dataPath = '/media/damiauzo/Kidney/TRI_PAS_data'

#DRL - path to write the masks
outPath = '../Output/slideChannels'

#DRL - level to use to generate mask
MASKLEVEL = -2

#DRL - if the outPath doesn't exist, create it
if not os.path.exists(outPath):
    os.makedirs(outPath)

#DRL - get a list of each slide
slidePaths = glob.glob(os.path.join(dataPath, '*.ndpi'))

#DRL - create a single canvas to draw on
plt.figure(figsize=(12,9))

#DRL - loop through each slide
for i, slidePath in enumerate(slidePaths):

    #DRL - get the name of the current file
    currFile = slidePath.split('/')[-1]
    currName = currFile.split('.')[0]
    print 'generating mask for slide %d of %d: %s' % (i, len(slidePaths), currName)

    #DRL - read in the current slide
    try:
        slide = openslide.OpenSlide(slidePath)
    except:
        print '***BAD NDPI FILE*** %s' % currFile
        continue

    #DRL - get a thumbnail for current slide
    try:
        slidePlot = slide.read_region((0, 0), slide.level_count - 2, slide.level_dimensions[slide.level_count - 2])
    except:
        print '***CORRUPT JPEG DATA*** %s' % currFile
        continue

    #DRL - make sure the current figure is clear
    plt.clf()

    #DRL - plot 3 different color schemes of the image and save it
    pUtils.plotColorSchemes(slidePlot)
    plt.savefig(os.path.join(outPath, currName + '_channels.png'))

    #DRL - make sure the current figure is clear
    plt.clf()

    #DRL - plot histograms of various color schemes and save it
    pUtils.plotSchemeHists(slidePlot)
    plt.savefig(os.path.join(outPath, currName + '_hists.png'))