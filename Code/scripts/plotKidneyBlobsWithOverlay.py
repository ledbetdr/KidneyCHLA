"""
LVH - driver script to extract kidney segmentations, bloblify and extract
minimum rectangle enclosing the blobs. Then it is plotted over the original
image.

Apply mask after rotation!
"""
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import os
import openslide
from skimage import measure as skmeasure
from skimage import color as skcolor
from skimage import transform as skt
import glob


plt.ioff()  # turn off interactive plotting
# LVH - local imports
import sys
sys.path.insert(0, "../")
from utils import segmentUtils as su

# LVH - set up paths
dataDir = r'/media/damiauzo/Projects/VPICU/Data/Kidney/TRI_PAS_data'
runName = 'segmentBlobOverlaysv12'
outputDir = os.path.join('../../Output/', runName)
if not os.path.exists(outputDir):
    os.mkdir(outputDir)

# LVH - load up slides
slidePaths = glob.glob(os.path.join(dataDir, '*PAS.ndpi'))
slidePaths = [os.path.join(dataDir, '10_26609_025_501 L4 PAS.ndpi')]
for it, slidePath in enumerate(slidePaths):
    uid = os.path.basename(slidePath).split('.')[0]
    print 'working on %i out of %i: %s' % (it, len(slidePaths), uid)

    print '... loading slide'
    slide = openslide.OpenSlide(slidePath)

    print '... segmenting image'
    image, segMask = su.segmentKidney(slide)
    image = np.array(image.convert("RGB"))
    height, width = image.shape[:2]

    print '... labeling blobs'
    label_image = skmeasure.label(segMask)
    image_label_overlay = skcolor.label2rgb(label_image, image=image)

    # LVH - set up figures
    plt.figure(2, figsize=(18, 14))
    plt.clf()
    fig, ax2 = plt.subplots(ncols=1, nrows=1, figsize=(18, 14), num=2)

    print '... getting blobs'
    # LVH - array to store all the blobs, rotated
    allRot = np.zeros(label_image.shape, dtype=np.int64)
    rects1 = []  # LVH - array to store the bounding box rectangles
    rects2 = []  # LVH - "^" of rotated images
    for it, region in enumerate(skmeasure.regionprops(label_image)):
        # LVH - get centroid of blobs & the corners
        y0, x0 = region.centroid
        orientation = region.orientation
        x1 = x0 + np.cos(orientation) * 0.5 * region.major_axis_length
        y1 = y0 - np.sin(orientation) * 0.5 * region.major_axis_length
        x2 = x0 - np.sin(orientation) * 0.5 * region.minor_axis_length
        y2 = y0 - np.cos(orientation) * 0.5 * region.minor_axis_length

        # LVH - plot the major / minor axis
        ax2.plot((x0, x1), (y0, y1), '-r', linewidth=2.0, alpha=0.3)
        ax2.plot((x0, x2), (y0, y2), '-r', linewidth=2.0, alpha=0.3)
        ax2.plot(x0, y0, '.g', markersize=15)

        # LVH - get the rectangle
        minr, minc, maxr, maxc = region.bbox
        rect1 = mpatches.Rectangle((minc, minr), maxc - minc, maxr - minr,
                                   fill=False, color='red', linewidth=1,
                                   alpha=0.3)
        rects1 += [rect1]

        # LVH - prepare to rotate the blobs by making new mask with only that blob activated
        #     - we want to rotate it to have major/minor axis to be aligned with x-y. This makes bounding box easy.
        newMask = np.zeros(label_image.shape, dtype=np.uint8)
        newMask[label_image == region.label] = 1

        # LVH - rotate it by the skimage detected orientation
        angle = np.rad2deg(-orientation)
        if np.abs(angle) > 45:  #LVH - if blob is oriented in a way that should be vertical
            angle = angle - np.sign(angle)*90

        # LVH - rotate image and make into binary since skimage's rotate does something funny
        rot = skt.rotate(newMask, angle, center=(x0, y0))
        rot[rot >= 0.002] = 1
        rot[rot <  0.002] = 0

        # LVH - store the rotated blob into a larger list of blobs, so we can overlay the blob rotations
        #     - over the original image and see how it looks. Basically a new "label_image" with rotated blobs
        allRot[(rot == 1) & (allRot == 0)] = it + 1

        # LVH - label the newly, x-y aligned blobs.
        lbl = skmeasure.label(rot)

        # LVH - region... within a region. Do it again to use regionprop property of "bbox" to easily extract the
        #     - minimum rectangle about the blob
        prop = skmeasure.regionprops(lbl)[0]

        # LVH - get the new, minimum area rectangle enclosing the rotated blob
        minr, minc, maxr, maxc = prop.bbox
        rect2 = mpatches.Rectangle((minc, minr), maxc - minc, maxr - minr,
                                   fill=False, edgecolor='green', linewidth=2)
        rects2 += [rect2]

    #LVH - plot the rotated blob mask, labeled, over the image as an overlay
#    imglblover = skcolor.label2rgb(allRot, image=image, image_alpha = 1.0, alpha = 0.7, bg_label = 0)
    imglblover = skcolor.label2rgb(label_image, image=image, image_alpha = 1.0, alpha = 0.7, bg_label = 0)

    ax2.imshow(imglblover)

    #LVH - plot the rectangles of the rotated major/minor axis enclosing space
#    for rect in rects2:
#        ax2.add_patch(rect)
    for rect in rects1: #LVH - now the original major/minor axis enclosing space
        ax2.add_patch(rect)

    #LVH - save plot
    ax2.set_xlim((0, width))
    ax2.set_ylim((height, 0))
    plt.axis('off')
    plt.savefig(os.path.join(outputDir, '%s.png'%uid), transparent = True, bbox_inches = 'tight')