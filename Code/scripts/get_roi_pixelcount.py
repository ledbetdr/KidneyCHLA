"""
Script to get the pixel count per slide of extracted regions of interest
"""
import os
import glob
from PIL import Image
import pandas as pd

roiDir = '../../Data/TRI_PAS_Data/ROI'
roiPaths = glob.glob(os.path.join(roiDir, '*.tif'))

for idx, roiPath in enumerate(roiPaths):
    img = Image.open(roiPath)
    width, height = img.size
    tmpDF = pd.DataFrame([[os.path.basename(roiPath), width, height]],
                         columns=['datapath', 'width', 'height'])

    if idx == 0:
        roiDF = tmpDF
    else:
        roiDF = pd.concat([roiDF, tmpDF])

roiDF['slideuid'] = roiDF['datapath'].apply(
                        lambda x: '_'.join(x.split('_')[:-1]))
slideDF = roiDF.groupby('slideuid').sum()
