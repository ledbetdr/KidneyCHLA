# -*- coding: utf-8 -*-
"""
Created on Sat Sep 26 02:29:36 2015

@author: damiauzo

DRL - quick script to rip out the highest decimation view of each slide
        for quick assessment
"""
import os
import glob
import openslide
import matplotlib.pyplot as plt

#DRL - path to the main directory containing the ndpi files
dataPath = '/media/damiauzo/Kidney/rawData'

#DRL - path to write the thumbnails
outPath = '../Output/slideThumbs'

#DRL - if the outPath doesn't exist, create it
if not os.path.exists(outPath):
    os.makedirs(outPath)

#DRL - get a list of each slide
slidePaths = glob.glob(os.path.join(dataPath, '*.ndpi'))

#DRL - create a single figure we'll plot to
plt.figure()

#DRL - keep track of bad image files
badFiles = []

#DRL - loop through each slide
for i, slidePath in enumerate(slidePaths):

    #DRL - get the name of the current file
    currFile = slidePath.split('/')[-1]
    currName = currFile.split('.')[0]
    print 'generating thumb for slide %d of %d: %s' % (i, len(slidePaths), currName)
    
    #DRL - make sure the current figure is clear
    plt.clf()
    
    #DRL - read in the current slide
    try:
        slide = openslide.OpenSlide(slidePath)
    except:
        print '***BAD NDPI FILE*** %s' % currFile
        badFiles.append(currFile)
        continue
    
    #DRL - get a thumbnail for current slide
    try:
        slidePlot = slide.read_region((0, 0), slide.level_count - 1, slide.level_dimensions[slide.level_count - 1])
    except:
        print '***CORRUPT JPEG DATA*** %s' % currFile
        badFiles.append(currFile)
        continue
    
    #DRL - plot the current slide
    plt.imshow(slidePlot)
    
    #DRL - save the most downsampled version of the image
    plt.savefig(os.path.join(outPath, currName + '.png'))
    
print 'there were %d bad files - check badFiles for more info' % len(badFiles)