"""
LVH - driver script to segment kidneys, extract blobs, and output rotated,
minimally bounded blobs to npy arrays

@author: longubu
"""
import os
import glob
import openslide
import numpy as np
from PIL import Image
import gc
import pandas as pd

# LVH - local imports
import sys
sys.path.insert(0, "../")
from utils import segmentUtils as su


# LVH - set up paths
# dataDir = r'/media/damiauzo/Projects/VPICU/Data/Kidney/TRI_PAS_data'
dataDir = '../../Data/PAS_data'
runName = 'KidenyBlobsv1'
outputDir = os.path.join('../../Data/', runName)
if not os.path.exists(outputDir):
    os.mkdir(outputDir)

pngDir = os.path.join(outputDir, 'png')
if not os.path.exists(pngDir):
    os.mkdir(pngDir)

npyDir = os.path.join(outputDir, 'npy')
if not os.path.exists(npyDir):
    os.mkdir(npyDir)

# LVH - load up slides
SLIDELVL = 1  # slide level dimensions that we want to extract kidney blobs
slidePaths = glob.glob(os.path.join(dataDir, '*PAS*ndpi'))

progressTrackerFile = os.path.join(outputDir, 'finishedSlides.csv')
if os.path.exists(progressTrackerFile):
    progressDF = pd.read_csv(progressTrackerFile)
else:
    progressDF = pd.DataFrame(slidePaths, columns=['slidePath'])
    progressDF['progress'] = 'Not_Finished'

slidePaths = progressDF[progressDF['progress'] == 'Not_Finished']['slidePath'].values
progressDF.set_index('slidePath', inplace=True)
for it, slidePath in enumerate(slidePaths):
    size = float(os.path.getsize(slidePath)) / 1000. / 1000.
    uid = os.path.basename(slidePath).split('.')[0]

    if size > 600:
        print 'skipping %i out of %i: %s' % (it, len(slidePaths), uid)
        progressDF.loc[slidePath, 'progress'] = 'SIZE_TOO_LARGE'
        progressDF.to_csv(progressTrackerFile)
        continue

    print 'working on %i out of %i: %s' % (it, len(slidePaths), uid)

    print '... loading slide'
    slide = openslide.OpenSlide(slidePath)

    print '... segmenting image'
    image, segMask = su.segmentKidney(slide)

    print '... upsampling mask'

    width, height = slide.level_dimensions[SLIDELVL]
    segMask2 = su.resizeImage(segMask, (width, height), resample=2)

    try:
        print '... extracing blob masks'
        blobs = su.extractMaskBlobs(slide, segMask2, SLIDELVL)
        nblobs = len(blobs)
        for blobit, blob in enumerate(blobs):
            print '... ... on blob %i of %i' % (blobit + 1, nblobs)
            blobimg = Image.fromarray(blob)
            blobimg.save(os.path.join(pngDir, '%s_%02d.png'
                                              % (uid, blobit + 1)))

            np.save(os.path.join(npyDir, '%s_%02d.npy' % (uid, blobit + 1)),
                    np.array(blobimg))

        progressDF.loc[slidePath, 'progress'] = 'Finished'
        del blobimg, blobs, blobit, blob, nblobs
    except MemoryError:
        progressDF.loc[slidePath, 'progress'] = 'Mem_Error'
        print '... Error due to insufficient memory. Skipping this one.'
    finally:
        del segMask, width, height, image, slide
        gc.collect()
        progressDF.to_csv(progressTrackerFile)
